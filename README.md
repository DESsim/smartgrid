## Use
```
python run.py --power --programme --phasing --emissions_factors --solar_pv_production --ev_schedules --weather --network_path --network_type --network_geometry --systems --output_file_path

```

## Sample use
```
python run.py --power sampledata/climelioth.parquet --solar_pv_production sampledata/solar_pv_production.csv --ev_schedules sampledata/electric_vehicles.csv --programme sampledata/programme.xlsx --phasing sampledata/phasage.xlsx --emissions_factors sampledata/emissions_factors.csv --weather sampledata/H1a.csv --network_path "\\ter3ficw2k01\DATA\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\reseau\geometrie\out" --network_type base --network_geometry 0000-TRA_base --systems "\\ter3ficw2k01\DATA\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\production\pole_energie\PROD_base\systems.json" --output_file_path test.csv

```

## Installation
Requires:
- pypsa
- glpk
- xarray
- geopandas

They can all be installed with conda

The path to the glpk solver is set into a json file under data/config.json
You can use the template data/json_template.config
