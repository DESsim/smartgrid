from .components.bus import Bus, HeatBus
from .component_builder import ComponentBuilder

import logging
import numpy as np

class Network:

    def __init__(self, smartgrid, name):
        self.smartgrid = smartgrid
        self.name = name
        self.components = {}
        self.buses = {}
        self.constraints = []

    def to_id(self, var):
        return self.name + "." + var

    def add_buses(self, buses):
        for bus in buses:
            self.add_bus(bus)

    def add_bus(self, bus):
        """
        bus type : one of electricity, heating, hot_water, cooling
        temperature : bus temperature in celsius degrees (if the bus is a heating, hot_water or cooling bus)
        """

        name = bus['name']

        # Thermal buses
        if 'temperature' in bus.keys() and "rh" not in bus.keys() and "return_temperature" not in bus.keys():
            self.buses[name] = HeatBus(
                network = self,
                name = bus['name'],
                temperature = bus['temperature']
            )
            
        elif 'return_temperature' in bus.keys() and "rh" not in bus.keys():
            self.buses[name] = HeatBus(
                network = self,
                name = bus['name'],
                temperature = bus['temperature'],
                return_temperature = bus['return_temperature'],
            )

        elif 'rh' in bus.keys():
            self.buses[name] = HeatBus(
                network = self,
                name = bus['name'],
                temperature = bus['temperature'],
                rh = bus['rh']
            )

        # Not thermal buses
        else:
            self.buses[name] = Bus(
                network = self,
                name = bus['name']
            )

        return self.buses[name]

    def add_components(self, components):
        for component in components:

            if 'component_type' not in component.keys():
                raise ValueError('No component type provided for component ' + component['name'] + '. Should be system or load.')

            c = ComponentBuilder().build(self, component)
            self.components[ c.name ] = c

    def add_constraints(self, constraint):
        self.constraints.append(constraint)

        
    def set_pypsa_costs(self, primary_energy_weight, ghg_emissions_weight, monetary_weight):
        for c, component in self.components.items():
            component.set_pypsa_costs(primary_energy_weight, ghg_emissions_weight, monetary_weight)



    def compute_costs(self, metric):

        total_network_marginal_cost = 0
        total_network_capital_cost = 0

        for c, component in self.components.items():
            marginal_cost, capital_cost = component.compute_costs(metric)
            total_network_capital_cost += capital_cost
            total_network_marginal_cost    += marginal_cost

        return total_network_capital_cost, total_network_marginal_cost

    def extract_results(self):
        for c, component in self.components.items():
            component.extract_results()
            component.add_to_flowbased()



