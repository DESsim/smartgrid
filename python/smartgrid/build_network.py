import pypsa
import numpy as np
import pandas as pd
import logging

def create_network(reseau, network_parameters, systems, emissions_factors, ext_solar_pv_power, weather, power, losses=""):

    # Extend PyPSA to allow links between more than 3 buses (elec + heat + cool for example)
    override_component_attrs = pypsa.descriptors.Dict({k : v.copy() for k,v in pypsa.components.component_attrs.items()})

    override_component_attrs["Link"].loc["bus2"] = ["string",np.nan,np.nan,"2nd bus","Input (optional)"]
    override_component_attrs["Link"].loc["efficiency2"] = ["static or series","per unit",1.,"2nd bus efficiency","Input (optional)"]
    override_component_attrs["Link"].loc["p2"] = ["series","MW",0.,"2nd bus output","Output"]

    # Set up PyPSA
    network = pypsa.Network(override_component_attrs=override_component_attrs)
    network.set_snapshots(range(8760))

    
    # Smart grid
    # -----------------------------------------
    # Electricity loop
    # Create the unique electricity network
    network.add("Bus", name="elec_loop")
    elec_ef = emissions_factors['enedis_ef'].round(3)

    if systems['regulation_mode'] == 'carbon':
        elec_marginal_cost = elec_ef
    else:
        elec_marginal_cost = 0.5*(elec_ef - elec_ef.min())/(elec_ef.max() - elec_ef.min())

    network.add("Generator",
                name="elec_network",
                bus="elec_loop",
                p_nom = 100000,
                marginal_cost=elec_marginal_cost)
    
    network.add("Generator",
                name="elec_surplus",
                bus="elec_loop",
                p_min_pu = -1,
                p_max_pu = 0,
                p_nom = 100000,
                marginal_cost= -elec_marginal_cost)
    
    # Solar PV on the cinema
    network.add("Generator",
                name="pathe_pv",
                bus="elec_loop",
                p_nom = ext_solar_pv_power.max(),
                p_max_pu=(ext_solar_pv_power/ext_solar_pv_power.max()).round(3).tolist())


    netnames = reseau["Sousreseau"].unique().tolist()

    # Loop on networks
    for netname in netnames:

        logging.info('--------')
        logging.info('Building network ' + netname)

        # Append the name of the network to the variable
        # added to the pypsa model
        def to_id(var):
            return netname + "_" + var

        # Add buses
        if 'fuel_cell' in systems.keys():
            if systems['fuel_cell']['networks'][netname]['enabled'] == True:
                network.add("Bus", name = to_id("biogas_loop"))

        if network_parameters['network_type'] == 'moderate_temperature':
            network.add("Bus", name = to_id("heat_loop"))
        else:
            network.add("Bus", name = to_id("heat_loop"))
            network.add("Bus", name = to_id("cool_loop"))

        # Losses
        if len(losses) > 0:

            if network_parameters['network_type'] == 'moderate_temperature':

                network.add("Load",
                    name = to_id("heat_losses"),
                    bus = to_id("heat_loop"),
                    p_set = np.round(losses.Mixed.values, 1))

                logging.info('Adding losses with maximum power ' + str(np.max(losses.Mixed.values)))

            else:

                network.add("Load",
                        name=to_id("cool_losses"),
                        bus=to_id("cool_loop"),
                        p_set = -np.round(losses.Cool.values, 1))

                logging.info('Adding losses for the cooling network with maximum power ' + str(np.max(losses.Cool.values)))

                network.add("Load",
                        name=to_id("heat_losses"),
                        bus=to_id("heat_loop"),
                        p_set = np.round(losses.Heat.values, 1))

                logging.info('Adding losses for the heating network with maximum power ' + str(np.max(losses.Heat.values)))
        
        
        # -----------------------------------------
        # Biogas / fuel cell
        if 'fuel_cell' in systems.keys():
            if systems['fuel_cell']['networks'][netname]['enabled'] == True:

                if systems['regulation_mode'] == 'carbon':
                    fuel_cell_marginal_cost = 0.0234
                else:
                    fuel_cell_marginal_cost = systems['fuel_cell']['merit_order'] + 0.5*0.0234

                network.add("Generator",
                            name = to_id("biogas_prod"),
                            bus = to_id("biogas_loop"),
                            marginal_cost = 0,
                            p_nom = 10000)
                
                network.add("Link",
                            name = to_id("fuel_cell"),
                            bus0 = to_id("biogas_loop"),
                            bus1 = to_id("heat_loop"), bus2 = "elec_loop",
                            efficiency = 0.7, efficiency2 = 0.3,
                            p_min_pu = 0, p_max_pu = 1,
                            p_nom = systems['fuel_cell']['networks'][netname]['nominal_power'],
                            marginal_cost = fuel_cell_marginal_cost)
        
        # -----------------------------------------
        # Heat loop
        # CPCU
        if 'cpcu' in systems.keys():
            if systems['cpcu']['networks'][netname]['enabled'] == True:

                cpcu_ef = emissions_factors['cpcu_ef'].round(3)

                if systems['regulation_mode'] == 'carbon':
                    cpcu_marginal_cost = cpcu_ef
                else:
                    cpcu_marginal_cost = systems['cpcu']['merit_order'] + 0.5*(cpcu_ef - cpcu_ef.min())/(cpcu_ef.max() - cpcu_ef.min())

                network.add("Generator",
                            name = to_id("cpcu"),
                            bus = to_id("heat_loop"),
                            marginal_cost = cpcu_marginal_cost,
                            p_nom = systems['cpcu']['networks'][netname]['nominal_power'])

                logging.info("Adding CPCU heat exchanger with nominal power : " + str(systems['cpcu']['networks'][netname]['nominal_power']))
        
        # Heat storage
        if 'heat_storage' in systems.keys():
            if systems['heat_storage']['networks'][netname]['enabled'] == True:

                network.add("StorageUnit",
                            name=to_id("heat_storage"),
                            bus=to_id("heat_loop"),
                            marginal_cost=0,
                            p_nom = systems['heat_storage']['networks'][netname]['nominal_power'],
                            max_hours = systems['heat_storage']['networks'][netname]['max_hours'])

                logging.info("Adding heat storage with nominal power : " + str(systems['heat_storage']['networks'][netname]['nominal_power']))

        # Gas boiler
        if 'gas_boiler' in systems.keys():
            if systems['gas_boiler']['networks'][netname]['enabled'] == True:

                if systems['regulation_mode'] == 'carbon':
                    gas_boiler_marginal_cost = 0.22
                else:
                    gas_boiler_marginal_cost = systems['gas_boiler']['merit_order'] + 0.5*0.22

                network.add("Generator",
                            name = to_id("gas_boiler"),
                            bus = to_id("heat_loop"),
                            p_nom = systems['gas_boiler']['networks'][netname]['nominal_power'],
                            marginal_cost = gas_boiler_marginal_cost)

                logging.info("Adding gas boiler with nominal power : " + str(systems['gas_boiler']['networks'][netname]['nominal_power']))
        


        # -----------------------------------------
        # Cool loop
        # Use climespace only if there is a cool loop, and use an aero cooler instead
        
        if network_parameters['network_type'] == 'moderate_temperature':

            if systems['regulation_mode'] == 'carbon':
                aero_marginal_cost = 0 # Already accounted for by the electricity emission factor
            else:
                aero_marginal_cost = systems['aero_cooler']['merit_order']

            aero_eer = 15

            network.add("Link",
                        name= to_id("aero_cooler"),
                        bus0 = "elec_loop",
                        bus1 = to_id("heat_loop"),
                        efficiency = -aero_eer,
                        p_min_pu = 0, p_max_pu = 1,
                        p_nom = systems['aero_cooler']['networks'][netname]['nominal_power']/aero_eer,
                        marginal_cost = aero_marginal_cost)

            logging.info("Adding aero cooler with nominal power : " + str(systems['aero_cooler']['networks'][netname]['nominal_power']))


        else:

            # Climespace
            climespace_ef = (emissions_factors['enedis_ef']/4.27).round(3)

            if systems['regulation_mode'] == 'carbon':
                climespace_marginal_cost = climespace_ef
            else:
                climespace_marginal_cost = systems['climespace']['merit_order']  + 0.5*(climespace_ef - climespace_ef.min())/(climespace_ef.max() - climespace_ef.min())

            network.add("Generator",
                        name = to_id("climespace"),
                        bus = to_id("cool_loop"),
                        marginal_cost=climespace_marginal_cost,
                        p_min_pu = -1, p_max_pu = 0,
                        p_nom=systems['climespace']['networks'][netname]['nominal_power'])

            logging.info("Adding climespace heat exchnager with nominal power : " + str(systems['climespace']['networks'][netname]['nominal_power']))


        # Cool storage
        if 'cool_storage' in systems.keys():
            if systems['cool_storage']['networks'][netname]['enabled'] == True:

                if network_parameters['network_type'] != 'moderate_temperature':

                    logging.info('Adding cool storage')

                    network.add("StorageUnit",
                                name = to_id("cool_storage"),
                                bus = to_id("cool_loop"),
                                marginal_cost=0,
                                p_min_pu = -1, p_max_pu = 0,
                                p_nom = systems['cool_storage']['networks'][netname]['nominal_power'],
                                max_hours = systems['cool_storage']['networks'][netname]['max_hours'])

                    logging.info("Adding cool storage with nominal power : " + str(systems['cool_storage']['networks'][netname]['nominal_power']))


        # -----------------------------------------
        # Heatpump linking heat and cool loops (TFP)
        if 'tfp' in systems.keys():
            if systems['tfp']['networks'][netname]['enabled'] == True:

                if network_parameters['network_type'] != 'moderate_temperature':

                    if systems['regulation_mode'] == 'carbon':
                        tfp_marginal_cost = 0 # Already accounted for by the electricity emission factor
                    else:
                        tfp_marginal_cost = systems['tfp']['merit_order']

                    # Compute the COP based on the model from https://www.researchgate.net/publication/255759857_A_review_of_domestic_heat_pumps
                    delta_temp = network_parameters['heating_loop_supply_temperature'] - network_parameters['cooling_loop_return_temperature']
                    cop_hp = 8.77 - 0.15*(delta_temp) + 0.000734*np.power(delta_temp, 2)
                    cop_hp = np.round(cop_hp, 1)

                    network.add("Link",
                                name = to_id("tfp"),
                                bus0 = "elec_loop",
                                bus1 = to_id("heat_loop"),
                                bus2 = to_id("cool_loop"),
                                efficiency = cop_hp, efficiency2 = -(cop_hp-1),
                                p_min_pu = 0, p_max_pu = 1,
                                p_nom = systems['tfp']['networks'][netname]['nominal_power'],
                                marginal_cost = tfp_marginal_cost)

                    logging.info("Adding a TFP with electrical nominal power : " + str(systems['tfp']['networks'][netname]['nominal_power']))

        
        # -----------------------------------------
        # Heatpump on air
        # Heating
        if 'air_heatpump' in systems.keys():
            if systems['air_heatpump']['networks'][netname]['enabled'] == True:

                if systems['regulation_mode'] == 'carbon':
                    air_heatpump_marginal_cost = 0 # Already accounted for by the electricity emission factor
                else:
                    air_heatpump_marginal_cost = systems['air_heatpump']['merit_order']

                # Compute the COP based on the model from https://www.researchgate.net/publication/255759857_A_review_of_domestic_heat_pumps
                air_temp = weather['air_temperature'].values - 273.15
                delta_temp = network_parameters['heating_loop_supply_temperature'] - air_temp
                cop_hp = 6.81 - 0.121*(delta_temp) + 0.00063*np.power(delta_temp, 2)
                cop_hp = np.round(cop_hp, 1)

                network.add("Link",
                            name = to_id("air_heatpump"),
                            bus0 = "elec_loop",
                            bus1 = to_id("heat_loop"),
                            efficiency = cop_hp,
                            p_min_pu = 0, p_max_pu = 1,
                            p_nom = systems['air_heatpump']['networks'][netname]['nominal_power']/cop_hp.mean(),
                            marginal_cost = air_heatpump_marginal_cost)

                logging.info("Adding a air heatpump with nominal power : " + str(systems['air_heatpump']['networks'][netname]['nominal_power']))

        # Cooling
        if 'chiller' in systems.keys():
            if systems['chiller']['networks'][netname]['enabled'] == True:

                if network_parameters['network_type'] != 'moderate_temperature':

                    if systems['regulation_mode'] == 'carbon':
                        chiller_marginal_cost = 0 # Already accounted for by the electricity emission factor
                    else:
                        chiller_marginal_cost = systems['chiller']['merit_order']

                    # Compute the COP based on the model from https://www.researchgate.net/publication/255759857_A_review_of_domestic_heat_pumps
                    air_temp = weather['air_temperature'].values - 273.15
                    delta_temp = air_temp - network_parameters['cooling_loop_supply_temperature']
                    cop_hp = 6.81 - 0.121*(delta_temp) + 0.00063*np.power(delta_temp, 2)
                    cop_hp = np.where(delta_temp < 0, 6.81, cop_hp)
                    cop_hp = np.round(cop_hp, 1)

                    network.add("Link",
                                name= to_id("chiller"),
                                bus0 = "elec_loop",
                                bus1 = to_id("cool_loop"),
                                efficiency = -(cop_hp-1),
                                p_min_pu = 0, p_max_pu = 1,
                                p_nom = systems['chiller']['networks'][netname]['nominal_power']/(cop_hp-1).mean(),
                                marginal_cost = chiller_marginal_cost)

                    logging.info("Adding a chiller with nominal power : " + str(systems['chiller']['networks'][netname]['nominal_power']))


        # -----------------------------------------
        # Geothermal heatpump
        if 'geothermal_heatpump_heating' in systems.keys():
            if systems['geothermal_heatpump_heating']['networks'][netname]['enabled'] == True:

                if systems['regulation_mode'] == 'carbon':
                    geothermal_heatpump_marginal_cost = 0 # Already accounted for by the electricity emission factor
                else:
                    geothermal_heatpump_marginal_cost = systems['geothermal_heatpump_heating']['merit_order']
               
                delta_temp = network_parameters['heating_loop_supply_temperature'] - 12
                cop_hp = 8.77 - 0.15*(delta_temp) + 0.000734*np.power(delta_temp, 2)
                cop_hp = np.round(cop_hp, 1)

                # Force heating to zero in summer / cooling to zero in winter
                # cooling season : 15th of april > 15th of october
                geo_heating = np.ones(8760)
                geo_heating[2543:6935] = 0.0

                network.add("Link",
                                name= to_id("geothermal_heatpump_heating"),
                                bus0 = "elec_loop",
                                bus1 = to_id("heat_loop"),
                                efficiency = cop_hp,
                                p_min_pu = 0, p_max_pu = geo_heating,
                                p_nom = systems['geothermal_heatpump_heating']['networks'][netname]['nominal_power']/cop_hp.mean(),
                                marginal_cost = geothermal_heatpump_marginal_cost)



        if 'geothermal_heatpump_cooling' in systems.keys():
            if systems['geothermal_heatpump_cooling']['networks'][netname]['enabled'] == True:

                if systems['regulation_mode'] == 'carbon':
                    geothermal_heatpump_marginal_cost = 0 # Already accounted for by the electricity emission factor
                else:
                    geothermal_heatpump_marginal_cost = systems['geothermal_heatpump_cooling']['merit_order']

                delta_temp = network_parameters['cooling_loop_supply_temperature'] - 12
                cop_hp = 8.77 - 0.15*(delta_temp) + 0.000734*np.power(delta_temp, 2)
                cop_hp = np.round(cop_hp, 1)
            

                # Force heating to zero in summer / cooling to zero in winter
                # cooling season : 15th of april > 15th of october

                geo_cooling = np.zeros(8760)
                geo_cooling[2543:6935] = 1.0

                network.add("Link",
                                name= to_id("geothermal_heatpump_cooling"),
                                bus0 = "elec_loop",
                                bus1 = to_id("heat_loop"), #keep heat_loop
                                efficiency = -(cop_hp-1),
                                p_min_pu = 0, p_max_pu = geo_cooling,
                                p_nom = systems['geothermal_heatpump_cooling']['networks'][netname]['nominal_power']/cop_hp.mean(),
                                marginal_cost = geothermal_heatpump_marginal_cost)



    
    return network


def add_building_to_network(network, network_parameters, building_id, network_id, heat_load, hot_water_load, cool_load, elec_load, p_pv):

    distribution_losses = 0.05
    heat_exchanger_losses = 0.01

    cool_energy = (-cool_load).sum()
    heat_energy = (heat_load + hot_water_load).sum()

    logging.info('Max heating power : ' + str(np.max(heat_load + hot_water_load)))
    logging.info('Max cooling power : ' + str(np.max(-cool_load)))

    # Prepend the name of the building to the variable
    # added to the pypsa model
    def to_id(var):
        return building_id + "_" + var
    
    # ---------------------
    # Internal power buses
    network.add("Bus", name=to_id("elec_loop"))
    network.add("Bus", name=to_id("heat_loop"))
    network.add("Bus", name=to_id("hot_water_loop"))
    network.add("Bus", name=to_id("cool_loop"))
    
    # ----------------------         
    # Electricity
    # Connexion to the electricity loop 
    network.add("Link",
                name=to_id("elec_connexion"),
                bus0 = "elec_loop",
                bus1 = to_id("elec_loop"),
                p_min_pu = -1, p_max_pu = 1, p_nom = 10000)

    # Electricity demand
    network.add("Load",
                name=to_id("elec_load"),
                bus=to_id("elec_loop"),
                p_set = elec_load)
    
    # ---------------------
    # Solar PV production
    p_pv = pd.Series(p_pv)
    pv_energy = p_pv.sum()
    
    if pv_energy > 0:

        network.add("Generator",
            name=to_id("pv"),
            bus=to_id("elec_loop"),
            p_nom = p_pv.max(),
            p_max_pu= (p_pv/p_pv.max()).round(3).tolist())
    
    # ---------------------
    # Heating
    network.add("Load",
                name=to_id("heat_load"),
                bus=to_id("heat_loop"),
                p_set = heat_load/(1-distribution_losses)/(1-heat_exchanger_losses))

    network.add("Load",
                name=to_id("hot_water_load"),
                bus=to_id("hot_water_loop"),
                p_set = hot_water_load/(1 - distribution_losses)/(1 - heat_exchanger_losses))


    # If the network has a moderate temperature
    if network_parameters['network_type'] == 'moderate_temperature':
#        print("Moderate temperature")
        # Raise the temperature to 45°C
        # First by using available heat on the cooling bus with a TFP
        # (if there is sufficient synchronous heating / cooling needs)

        # Compute the COP of the TFP (heat source : 10°C, heat sink : 45°C)
        delta_temp = 45 - 10
        cop_tfp = 8.77 - 0.15*(delta_temp) + 0.000734*np.power(delta_temp, 2)
        cop_tfp = np.round(cop_tfp, 1)

        # Compute the potential load factor
        # p_synchro = np.minimum((cop_tfp-1)/cop_tfp*heat_load, -cool_load)
        # p_tfp_max = np.quantile(p_synchro, 0.8)      
        # if p_tfp_max > 0.0:
        #     p_tfp = np.minimum(p_synchro, p_tfp_max)
        #     load_factor = np.sum(p_tfp)/(p_tfp_max*8760)
        #     if load_factor > 0.25:
        #         print("Adding TFP to {}".format(building_id))
                
        network.add("Link",
                            name = to_id("tfp"),
                            bus0 = to_id("elec_loop"),
                            bus1 = to_id("heat_loop"),
                            bus2 = to_id("cool_loop"),
                            efficiency = cop_tfp, efficiency2 = -(cop_tfp-1),
                            p_min_pu = 0, p_max_pu = 1, p_nom = 10000)


        # Second by taking heat available on the network with a heatpump
        # Compute the COP of the heatpump (heat source : 20 - 30°C, heat sink : 45°C)
        delta_temp = 45 - (network_parameters['heating_loop_supply_temperature'] - 2)
        cop_hp = 8.77 - 0.15*(delta_temp) + 0.000734*np.power(delta_temp, 2)
        cop_hp = np.round(cop_hp, 1)

        network.add("Link",
                    name = to_id("heat_hp"),
                    bus0 = to_id("elec_loop"),
                    bus1 = network_id + '_' + "heat_loop",
                    bus2 = to_id("heat_loop"),
                    efficiency = -(cop_hp-1), efficiency2 = cop_hp,
                    p_min_pu = 0, p_max_pu = 1, p_nom = 10000)

    else:

        # If the temperature is high enough for heating systems (> 45 °C)
        # use a direct connexion with the network
        network.add("Link",
                    name = to_id("heat_con"),
                    bus0 = network_id + '_' + "heat_loop",
                    bus1 = to_id("heat_loop"),
                    efficiency = 1,
                    p_min_pu = 0, p_max_pu = 1, p_nom = 10000)


    # ---------------------
    # Hot water
    # Raise the temperature of the heat bus to 60°C
    # Compute the COP of the heatpump (heat source : 45°C, heat sink : 60°C)
    delta_temp = 60 - 45
    cop_hp = 8.77 - 0.15*(delta_temp) + 0.000734*np.power(delta_temp, 2)
    cop_hp = np.round(cop_hp, 1)

    network.add("Link",
                name = to_id("heat_hot_water_con"),
                bus0 = to_id("elec_loop"),
                bus1 = to_id("heat_loop"),
                bus2 = to_id("hot_water_loop"),
                efficiency = -(cop_hp-1), efficiency2 = cop_hp,
                p_min_pu = 0, p_max_pu = 1, p_nom = 10000)


    
    
    # ---------------------
    # Heat storage
    e_sto = 0
    if building_id == 'B1D1.logements':
        e_sto = 160
    elif building_id == 'B1C1.logements':
        e_sto = 35
    elif building_id == 'B2A.tour_seine':
        e_sto = 160
    
    if e_sto > 0:
        network.add("StorageUnit",
                name=to_id("heat_storage"),
                bus=to_id("heat_loop"),
                marginal_cost=0,
                p_nom = e_sto/2, max_hours = 2)

    
    # ---------------------
    # Cooling
    
    # Only if the building needs cooling
    if cool_energy > 1e-3:

        # If the network has a moderate temperature
        if network_parameters['network_type'] == 'moderate_temperature':

            delta_temp = (network_parameters['cooling_loop_supply_temperature']+2) - 10
            cop_hp = 8.77 - 0.15*(delta_temp) + 0.000734*np.power(delta_temp, 2)
            cop_hp = np.round(cop_hp, 1)

            # Lower the temperature to 10°C with a heatpump
            network.add("Link",
                        name = to_id("cool_hp"),
                        bus0 = to_id("elec_loop"),
                        bus1 = network_id + '_' + "heat_loop",
                        bus2 = to_id("cool_loop"),
                        efficiency = cop_hp, efficiency2 = -(cop_hp-1),
                        p_min_pu = 0, p_max_pu = 1, p_nom = 10000)

        
        else:

            # If the temperature is low enough for cooling systems (< 10 °C)
            # use a direct connexion with the network
            network.add("Link",
                        name=to_id("cool_con"),
                        bus0 = network_id + '_' + "cool_loop",
                        bus1 = to_id("cool_loop"),
                        p_min_pu = -1, p_max_pu = 0, p_nom = 10000)


        # Cooling demand
        network.add("Load",
                    name=to_id("cool_load"),
                    bus=to_id("cool_loop"),
                    p_set = cool_load/(1 - distribution_losses)/(1 - heat_exchanger_losses))


def add_independant_building(network, network_parameters, systems, emissions_factors, weather, building_id, heat_load, hot_water_load, cool_load, elec_load, p_pv):

    distribution_losses = 0.05
    heat_exchanger_losses = 0.01

    cool_energy = (-cool_load).sum()
    heat_energy = (heat_load + hot_water_load).sum()

    logging.info('Max heating power : ' + str(np.max(heat_load + hot_water_load)))
    logging.info('Max cooling power : ' + str(np.max(-cool_load)))
    
    # Prepend the name of the building to the variable
    # added to the pypsa model
    def to_id(var):
        return building_id + "_" + var
    
    # ---------------------
    # Internal power buses
    network.add("Bus", name=to_id("elec_loop"))
    network.add("Bus", name=to_id("heat_loop"))
    network.add("Bus", name=to_id("hot_water_loop"))
    network.add("Bus", name=to_id("cool_loop"))
    
    # ----------------------         
    # Electricity
    # Connexion to the electricity loop 
    network.add("Link",
                name=to_id("elec_connexion"),
                bus0 = "elec_loop",
                bus1 = to_id("elec_loop"),
                p_min_pu = -1, p_max_pu = 1, p_nom = 10000)

    # Electricity demand
    network.add("Load",
                name=to_id("elec_load"),
                bus=to_id("elec_loop"),
                p_set = elec_load)
    
    # ---------------------
    # Solar PV production
    p_pv = pd.Series(p_pv)
    pv_energy = p_pv.sum()
    
    if pv_energy > 0:

        network.add("Generator",
            name=to_id("pv"),
            bus=to_id("elec_loop"),
            p_nom = p_pv.max(),
            p_max_pu= (p_pv/p_pv.max()).round(3).tolist())
    
    # ---------------------
    # Heating

    # Load
    network.add("Load",
                name=to_id("heat_load"),
                bus=to_id("heat_loop"),
                p_set = heat_load/(1-distribution_losses)/(1-heat_exchanger_losses))

    network.add("Load",
                name=to_id("hot_water_load"),
                bus=to_id("hot_water_loop"),
                p_set = hot_water_load/(1 - distribution_losses)/(1 - heat_exchanger_losses))

    # Production
    # Cite Kagan buildings are connected to CPCU
    if building_id in ['B2A.cite_kagan.a', 'B2A.cite_kagan.b', 'B2A.cite_kagan.c', 'B2A.cite_kagan.d', 'B2A.cite_kagan.e']:

        cpcu_ef = emissions_factors['cpcu_ef'].round(3)

        if systems['regulation_mode'] == 'carbon':
            cpcu_marginal_cost = cpcu_ef
        else:
            cpcu_marginal_cost = systems['cpcu']['merit_order'] + 0.5*(cpcu_ef - cpcu_ef.min())/(cpcu_ef.max() - cpcu_ef.min())

        network.add("Generator",
                    name=to_id("cpcu_heating"),
                    bus=to_id("heat_loop"),
                    marginal_cost=cpcu_marginal_cost,
                    p_nom=10000)

        network.add("Generator",
                    name=to_id("cpcu_hot_water"),
                    bus=to_id("hot_water_loop"),
                    marginal_cost=cpcu_marginal_cost,
                    p_nom=10000)


    # Other buildings are equipped with a TFP (if relevant) / air heatpump / gas boiler / chiller
    else:

        # Add a TFP if there is synchronous heating / cooling  needs

        # Compute the COP of the TFP (heat source : 10°C, heat sink : 45°C)
        delta_temp = 45 - 10
        cop_tfp = 8.77 - 0.15*(delta_temp) + 0.000734*np.power(delta_temp, 2)
        cop_tfp = np.round(cop_tfp, 1)

        # Compute the potential load factor
        p_synchro = np.minimum((cop_tfp-1)/cop_tfp*heat_load, -cool_load)
        p_tfp_max = np.quantile(p_synchro, 0.8)
        
        if p_tfp_max > 0.0:

            p_tfp = np.minimum(p_synchro, p_tfp_max)
            load_factor = np.sum(p_tfp)/(p_tfp_max*8760)

            if load_factor > 0.25:

                network.add("Link",
                            name = to_id("tfp"),
                            bus0 = to_id("elec_loop"),
                            bus1 = to_id("heat_loop"),
                            bus2 = to_id("cool_loop"),
                            efficiency = cop_tfp, efficiency2 = -(cop_tfp-1),
                            p_min_pu = 0, p_max_pu = 1, p_nom = 10000)


        # Add a air source heatpump to cover 75 % of the peak heating load
        air_temp = weather['air_temperature'].values - 273.15
        delta_temp = 45 - air_temp
        cop_hp = 6.81 - 0.121*(delta_temp) + 0.00063*np.power(delta_temp, 2)
        cop_hp = np.round(cop_hp, 1)

        network.add("Link",
                    name = to_id("air_heatpump"),
                    bus0 = to_id("elec_loop"),
                    bus1 = to_id("heat_loop"),
                    efficiency = cop_hp,
                    p_nom = np.quantile(heat_load, 0.75))

        # Add a gas boiler to cover the rest of the heating load
        if systems['regulation_mode'] == 'carbon':
            gas_boiler_marginal_cost = 0.22
        else:
            gas_boiler_marginal_cost = systems['gas_boiler']['merit_order'] + 0.5*0.22

        network.add("Generator",
                    name=to_id("gas_boiler_heating"),
                    bus=to_id("heat_loop"),
                    p_nom=10000,
                    marginal_cost= gas_boiler_marginal_cost)

        # Add a gas boiler to cover the hot water load
        network.add("Generator",
                    name=to_id("gas_boiler_hot_water"),
                    bus=to_id("hot_water_loop"),
                    p_nom=10000,
                    marginal_cost= gas_boiler_marginal_cost)


    
    # ---------------------
    # Heat storage
    e_sto = 0
    if building_id == 'B1D1.logements':
        e_sto = 160
    elif building_id == 'B1C1.logements':
        e_sto = 35
    elif building_id == 'B2A.tour_seine':
        e_sto = 160
    
    if e_sto > 0:
        network.add("StorageUnit",
                name=to_id("heat_storage"),
                bus=to_id("heat_loop"),
                p_nom = e_sto/2, max_hours = 2)

    # ---------------------
    # Cooling
    # Only if the building needs cooling
    if cool_energy > 1e-3:

        # Cooling demand
        network.add("Load",
                    name=to_id("cool_load"),
                    bus=to_id("cool_loop"),
                    p_set = cool_load/(1 - distribution_losses)/(1 - heat_exchanger_losses))

        # Compute the COP based on the model from https://www.researchgate.net/publication/255759857_A_review_of_domestic_heat_pumps
        air_temp = weather['air_temperature'].values - 273.15
        delta_temp = air_temp - 10
        cop_hp = 6.81 - 0.121*(delta_temp) + 0.00063*np.power(delta_temp, 2)
        cop_hp = np.where(delta_temp < 0, 6.81, cop_hp)
        cop_hp = np.round(cop_hp, 1)

        network.add("Link",
                    name= to_id("chiller"),
                    bus0 = to_id("elec_loop"),
                    bus1 = to_id("cool_loop"),
                    efficiency = -(cop_hp-1),
                    p_min_pu = 0, p_max_pu = 1,
                    p_nom = 10000)




def compute_electricity_load(power, building_id):

    b_id = building_id

    p_equi = power.loc[ power['building_id'] == b_id, 'p_equi']/1000
    p_equi = np.convolve(p_equi, [0.125, 0.75, 0.125], 'same')
        
    p_vent = power.loc[ power['building_id'] == b_id, 'p_vent']/1000
    p_vent = np.convolve(p_vent, [0.125, 0.75, 0.125], 'same')
        
    p_light = power.loc[ power['building_id'] == b_id, 'p_light']/1000
    p_light = np.convolve(p_light, [0.125, 0.75, 0.125], 'same')

    p_hw = power.loc[ power['building_id'] == b_id, 'p_hw']/1000
    p_hw = np.convolve(p_hw, [0.125, 0.75, 0.125], 'same')
        
    p_hw_recup = pd.Series(np.zeros(8760))
    if b_id in ['B1D1.logements', 'B1C1.logements', 'B2A.tour_seine']:
        p_hw_recup = p_hw*0.3 # 30% recup effficiency

    elec_load = p_equi + p_vent + p_light
    elec_load += p_hw_recup/3 # hot water recup heatpump with a COP = 3

    return elec_load


def compute_heat_load(power, building_id):
    b_id = building_id
    p_heat = (power.loc[ power['building_id'] == b_id, 'p_heat'] - power.loc[ power['building_id'] == b_id, 'p_heat_reco'])/1000
    p_heat = np.convolve(p_heat, [0.125, 0.75, 0.125], 'same')
    return p_heat

def compute_hot_water_load(power, building_id):
    b_id = building_id
    p_hw = power.loc[ power['building_id'] == b_id, 'p_hw']/1000
    p_hw = np.convolve(p_hw, [0.125, 0.75, 0.125], 'same')
        
    p_hw_recup = np.zeros(8760)
    if b_id in ['B1D1.logements', 'B1C1.logements', 'B2A.tour_seine']:
        p_hw_recup = p_hw*0.3 # 30% recup effficiency

    hot_water_load = p_hw - p_hw_recup

    return hot_water_load

def compute_cool_load(power, building_id):
    b_id = building_id
    p_cool = (power.loc[ power['building_id'] == b_id, 'p_cool'] - power.loc[ power['building_id'] == b_id, 'p_cool_reco'])/1000
    p_cool = np.convolve(p_cool, [0.125, 0.75, 0.125], 'same')
    return p_cool


def add_buildings(network, building_ids, reseau, network_parameters, systems, emissions_factors, weather, power):

    # Create lists of buildings per subnetwork
    netnames = reseau["Sousreseau"].unique().tolist()
    subnetwork_buildings = {}

    for netname in netnames:
        subnetwork = reseau.loc[reseau.Sousreseau == netname,:]
        origin = subnetwork[(subnetwork.Origin != "0")].Origin.unique().tolist()
        bldg_innet = subnetwork[(subnetwork.Destination != "0")].Destination.unique().tolist()
        bldg_innet += origin
        subnetwork_buildings[netname] = bldg_innet

    # Invert the dict to get the name of the subnetwork for each building
    buildings_subnetworks = {}
    for netname, b_ids in subnetwork_buildings.items():
        for b_id in b_ids:
            buildings_subnetworks[b_id] = netname
    
    for b_id in building_ids:
        
        elec_load = compute_electricity_load(power, b_id)
        heat_load = compute_heat_load(power, b_id)
        hot_water_load = compute_hot_water_load(power, b_id)
        cool_load = compute_cool_load(power, b_id)
        p_pv = power.loc[ power['building_id'] == b_id, 'p_pv']
            
        elec_load = elec_load.round(1)
        hot_water_load = hot_water_load.round(1)
        heat_load = heat_load.round(1)
        cool_load = cool_load.round(1)
        p_pv = p_pv.round(1)

        if b_id in buildings_subnetworks.keys():

            logging.info("adding " + b_id + " to network " + buildings_subnetworks[b_id])
        
            add_building_to_network(
                network,
                network_parameters,
                b_id,
                buildings_subnetworks[b_id],
                heat_load,
                hot_water_load,
                cool_load,
                elec_load,
                p_pv
            )

        else:

            logging.info("adding " + b_id + " as independant building")

            add_independant_building(
                network,
                network_parameters,
                systems,
                emissions_factors,
                weather,
                b_id,
                heat_load,
                hot_water_load,
                cool_load,
                elec_load,
                p_pv
            )


            
    return network



def add_electric_vehicles(network, emissions_factors, ev):

    # EVs
    network.add("Bus", name="elec_home_loop")
    network.add("Bus", name="work_ev_battery")
    network.add("Bus", name="ev_battery")

    network.add("Generator",
                name = "elec_network_home",
                bus = "elec_home_loop",
                p_nom = 10000,
                marginal_cost = (emissions_factors['enedis_ef']).round(3).tolist())

    N_ev = 180
    k_fois = 0.75

    # --------------------------------
    # Batteries
    network.add("Store",
                "work_ev_battery_storage",
                bus="work_ev_battery",
                e_nom=k_fois*N_ev/4*60, e_initial=k_fois*N_ev/4*60)

    network.add("Store",
                "ev_battery_storage",
                bus="ev_battery",
                e_nom=k_fois*N_ev*3/4*60, e_initial=k_fois*N_ev*3/4*60)


    # ---------------------------------
    # When the EVs are at work
    network.add("Link",
                name = "work_ev_charger_work",
                p_nom = k_fois*N_ev/4*7,
                bus0 = "B2A.cite_kagan.a_elec_loop",
                bus1 = "work_ev_battery",
                p_max_pu = ev["service_ev_at_work"].tolist())

    network.add("Link",
                name = "ev_charger_work",
                p_nom = k_fois*N_ev*3/4*7,
                bus0 = "B2A.cite_kagan.a_elec_loop",
                bus1 = "ev_battery",
                p_max_pu = ev["ev_at_work"].tolist())

    # ---------------------------------
    # When the EVs are at home
    network.add("Link",
                name = "ev_charger_home",
                p_nom = k_fois*N_ev*3/4*7,
                bus0 = "elec_home_loop",
                bus1 = "work_ev_battery",
                p_max_pu = ev["ev_at_home"].tolist())


    # ---------------------------------
    # 
    network.add("Load",
                name="work_ev_driving",
                bus = "work_ev_battery",
                p_set = (k_fois*N_ev/4*ev["service_ev_power"]).tolist())

    network.add("Load",
                name="ev_driving",
                bus = "ev_battery",
                p_set = (k_fois*N_ev*3/4*ev["power_ev"]).tolist())
    
    
    return network


from scipy.optimize import minimize_scalar



def autosize_systems(reseau, systems, power):

    if systems['autosize'] == True:

        def load_factor_diff(p_system, p_load, target_load_factor):
            p_prod = np.minimum(p_system, p_load)
            load_factor = np.sum(p_prod)/(p_system*8760)
            return (load_factor - target_load_factor)**2

        # Create lists of buildings per subnetwork
        netnames = reseau["Sousreseau"].unique().tolist()
        subnetwork_buildings = {}

        for netname in netnames:
            subnetwork = reseau.loc[reseau.Sousreseau == netname,:]
            origin = subnetwork[(subnetwork.Origin != "0")].Origin.unique().tolist()
            bldg_innet = subnetwork[(subnetwork.Destination != "0")].Destination.unique().tolist()
            bldg_innet += origin
            subnetwork_buildings[netname] = bldg_innet

        # For each subnetwork
        for subnet, building_ids in subnetwork_buildings.items():

            p_heat = []
            p_cool = []

            # Compute the global heating + hot_water / cooling loads 
            for b_id in building_ids:
                p_heat.append(compute_heat_load(power, b_id))
                p_heat.append(compute_hot_water_load(power, b_id))
                p_cool.append(compute_cool_load(power, b_id))

            p_heat = sum(p_heat)
            p_cool = sum(p_cool)

            # Handle the TFP cool > heat loops transfer
            if 'tfp' in systems.keys():
                if systems['tfp']['networks'][subnet]['enabled'] == True:
                    p_synchro = np.minimum(p_heat*3/4, -p_cool)

                    opt = minimize_scalar(
                        load_factor_diff,
                        bounds=(1.0, np.max(p_synchro)),
                        args=(p_synchro, systems['tfp']['networks'][subnet]['min_load_factor']),
                        method='bounded',
                        options={'xatol': 0.1}
                    )

                    # Verify that the optimization found a nominal power
                    # to match the required minimal load factor
                    # (and make it zero otherwise)
                    if opt['x'] > 2.0 and opt["fun"] > systems['tfp']['networks'][subnet]['min_load_factor']:
                        p_system = opt['x']
                    else:
                        p_system = 0

                    p_prod = np.minimum(p_system, p_synchro)
                    p_heat -= p_prod*4/3
                    p_cool += p_prod

                    systems['tfp']['networks'][subnet]['nominal_power'] = np.round(p_system/4)

            
            # Order systems by merit order
            heat_systems = ['cpcu', 'geothermal_heatpump_heating', 'air_heatpump']
            cool_systems = ['climespace', 'geothermal_heatpump_cooling']
            
            # ----------------------------
            # Heating systems
            heat_merit_order = {}
            order = []

            for h_sys in heat_systems:
                if h_sys in systems.keys():
                    system_properties = systems[h_sys]['networks'][subnet]
                    system_properties['system'] = h_sys
                    mo = systems[h_sys]['merit_order']
                    heat_merit_order[ mo ] = system_properties
                    order.append(mo)

            order = np.sort(order)

            # For each system following the merit order sequence
            for o in order:
                
                if heat_merit_order[o]['enabled']:
                    # Compute the power needed to approach the desired load factor
                    if heat_merit_order[o]['nominal_power'] == 'autosize':

                        if 'max_power' in heat_merit_order[o].keys():
                            max_power = heat_merit_order[o]['max_power']
                        else:
                            max_power = np.max(p_heat)

                        opt = minimize_scalar(
                                load_factor_diff,
                                bounds=(1.0, max_power),
                                args=(p_heat, heat_merit_order[o]['min_load_factor']),
                                method='bounded',
                                options={'xatol': 0.1}
                        )

                        
                        # Verify that the optimization found a nominal power
                        # to match the required minimal load factor
                        # (and make it zero otherwise)
                        if opt['x'] > 2.0 and opt["fun"] > heat_merit_order[o]['min_load_factor']:
                            p_system = opt['x']
                        else:
                            p_system = 0
    
                    # Or use the specified nominal power
                    else:
                        p_system = heat_merit_order[o]['nominal_power']
    
                    # Remove the estimated production from the load
                    p_prod = np.minimum(p_system, p_heat)
                    p_heat -= p_prod
                    
                    
                    # Modify the nominal power in the initial systems dict
                    systems[heat_merit_order[o]['system']]['networks'][subnet]['nominal_power'] = np.round(p_system)

            # ----------------------------
            # Cooling systems
            cool_merit_order = {}
            order = []

            for c_sys in cool_systems:
                if c_sys in systems.keys():
                    system_properties = systems[c_sys]['networks'][subnet]
                    system_properties['system'] = c_sys
                    mo = systems[c_sys]['merit_order']
                    cool_merit_order[ mo ] = system_properties
                    order.append(mo)

            order = np.sort(order)

            # For each system following the merit order sequence
            for o in order:
                # Compute the power needed to approach the desired load factor
                if cool_merit_order[o]['enabled']:
                    if cool_merit_order[o]['nominal_power'] == 'autosize':
    
                        if 'max_power' in cool_merit_order[o].keys():
                            max_power = cool_merit_order[o]['max_power']
                        else:
                            max_power = np.max(-p_cool)
    
                        opt = minimize_scalar(
                                load_factor_diff,
                                bounds=(1.0, np.max(-p_cool)),
                                args=(-p_cool, cool_merit_order[o]['min_load_factor']),
                                method='bounded',
                                options={'xatol': 0.01}
                        )

    
                        # Verify that the optimization found a nominal power
                        # to match the required minimal load factor
                        # (and make it zero otherwise)
                        if opt['x'] > 2.0 and opt["fun"] > cool_merit_order[o]['min_load_factor']:
                            p_system = -opt['x']
                        else:
                            p_system = 0
                            
                    # Or use the specified nominal power
                    else:
                        p_system = -cool_merit_order[o]['nominal_power']
    
                    # Remove the estimated production from the load
                    p_prod = np.maximum(p_system, p_cool)
                    p_cool += p_prod
    
                    # Modify the nominal power in the initial systems dict
                    systems[cool_merit_order[o]['system']]['networks'][subnet]['nominal_power'] = -np.round(p_system)

    return systems