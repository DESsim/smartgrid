from ..link import Link
import logging
import numpy as np

class Heatpump(Link):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "HeatPump"

        self.electricity_bus = properties['electricity_bus']


    def add_to_pypsa_network(self):

        logging.info("Adding " + self.name + " to pypsa")

        self.pypsa_sim.add(
            "Link",
            name = self.pypsa_name,
            bus0 = self.electricity_bus.pypsa_name,
            bus1 = self.source_bus.pypsa_name,
            bus2 = self.sink_bus.pypsa_name,
            efficiency = -(self.cop-1)*self.seasonal_factor,
            efficiency2 = self.cop*self.seasonal_factor,
            p_min_pu = 0.0,
            p_max_pu = 1.0
        )

        if self.nominal_power is not None:
            self.pypsa_sim.links.loc[self.pypsa_name, "p_nom"] = self.compressor_nominal_power

    

    def extract_results(self):

        vector = self.vector
              
        # Heat extraction from the source bus
        source_power = self.pypsa_sim.links_t.p1[self.pypsa_name].values

        self.power_inputs.append({
            'from': self.source_bus.pypsa_name,
            'to': self.pypsa_name,
            "vector": vector,
            'power': source_power
        })

        # Electrical power of the heatpump's compressor
        compressor_power = self.pypsa_sim.links_t.p0[self.pypsa_name].values

        self.power_inputs.append({
            'from': self.electricity_bus.pypsa_name,
            'to': self.pypsa_name,
            "vector" : "electricity",
            'power': compressor_power
        })

        # Heat injection in the sink bus
        sink_power = -self.pypsa_sim.links_t.p2[self.pypsa_name].values

        self.power_outputs.append({
            'from': self.pypsa_name,
            'to': self.sink_bus.pypsa_name,
            "vector": vector,
            'power': sink_power
        })


class AirWaterHeatpump(Heatpump):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "AirWaterHeatPump"

        self.set_costs_from_properties_file('heat_pump', 'air_water_heatpump')

        # ---------------------------------------
        # Compute the COP based on the model from https://www.researchgate.net/publication/255759857_A_review_of_domestic_heat_pumps
        delta_temp = self.sink_bus.temperature - self.source_bus.temperature
        cop = 6.81 - 0.121*(delta_temp) + 0.00063*np.power(delta_temp, 2)

        self.cop = np.round(np.ones(8760)*cop, 1)

        # The nominal power is defined at the exterior air temp of 7°C and the sink temp of 35°C
        cop_35_7 = 6.81 - 0.121*(35 - 7) + 0.00063*np.power(35 - 7, 2)

        if self.nominal_power is not None:
        	self.compressor_nominal_power = self.nominal_power/cop_35_7

        logging.info('Adding heat pump ' + self.name + ' with source bus ' + self.source_bus.name + ' and sink bus ' + self.sink_bus.name)
        self.add_to_pypsa_network()
        
        
class AdiabaticCooler(Heatpump):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "AdiabaticCooler"
        self.set_costs_from_properties_file('heat_pump', 'adiabatic_cooler')
        
        # Simple model from Simone's Excel worksheet
        cop=np.ones(8760)*35
        cop[self.sink_bus.temperature<23] = -2.2667*self.sink_bus.temperature[self.sink_bus.temperature<23] + 86.133
        self.cop = cop

        if self.nominal_power is not None:
        	self.compressor_nominal_power = self.nominal_power/np.mean(self.cop)
        
        logging.info('Adding adiabatic cooler ' + self.name + ' with source bus ' + self.source_bus.name + ' and sink bus ' + self.sink_bus.name)
        self.add_to_pypsa_network()
        
    def add_to_pypsa_network(self):

        logging.info("Adding " + self.name + " to pypsa")
        self.pypsa_sim.add(
            "Link",
            name = self.pypsa_name,
            bus0 = self.electricity_bus.pypsa_name,
            bus1 = self.source_bus.pypsa_name,
            bus2 = self.sink_bus.pypsa_name,
            efficiency = -(self.cop-1)*self.seasonal_factor,
            efficiency2 = self.cop*self.seasonal_factor,
            p_min_pu = 0.0,
            p_max_pu = 1.0
        )

        if self.nominal_power is not None:
            self.pypsa_sim.links.loc[self.pypsa_name, "p_nom"] = self.compressor_nominal_power
        


class WaterWaterHeatpump(Heatpump):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "WaterWaterHeatPump"

        self.set_costs_from_properties_file('heat_pump', 'water_water_heatpump')

        # ---------------------------------------
        # Compute the COP based on the model from https://www.researchgate.net/publication/255759857_A_review_of_domestic_heat_pumps
        delta_temp = self.sink_bus.temperature - self.source_bus.temperature
        cop = 8.77 - 0.15*(delta_temp) + 0.000734*np.power(delta_temp, 2)

        self.cop = np.round(np.ones(8760)*cop, 1)

        # The nominal power is defined at the exterior air temp of 7°C and the sink temp of 35°C
        cop_35_10 = 8.77 - 0.15*(35 - 10) + 0.000734*np.power(35 - 10, 2)

        if self.nominal_power is not None:
        	self.compressor_nominal_power = self.nominal_power/cop_35_10

        logging.info('Adding heat pump ' + self.name + ' with source bus ' + self.source_bus.name + ' and sink bus ' + self.sink_bus.name)
        self.add_to_pypsa_network()

class Thermorefrigeratingpump(WaterWaterHeatpump):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "Thermorefrigeratingpump"


class ThermorefrigeratingpumpWithAir(WaterWaterHeatpump):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "ThermorefrigeratingpumpWithAir"
        self.source_bus2 = properties['source_bus2']     
        self.makeAir(network,properties)
        self.constrained = True
        network.add_constraints(self.extra_functionality)

    def makeAir(self,network,properties):
        system = [{
                  'component_type': 'system', 
                  'name': self.name+"_air",
                  'system': 'air_water_heat_pump',
                  'source_bus': self.source_bus2,
                  'sink_bus': self.sink_bus,
                  'electricity_bus': self.electricity_bus,
                  'vector': self.vector,
                  'nominal_power': self.nominal_power
                }]

        network.add_components(system)

        self.tfp_air = network.components[self.name+"_air"]
        self.cop_air = self.tfp_air.cop
        self.seasonal_factor_air = self.tfp_air.seasonal_factor
        
    def extra_functionality(self,network,snapshots):
            from pyomo.environ import Constraint
            def tfpsum(model,snapshot):
                return model.link_p[self.pypsa_name,snapshot] * (self.cop*self.seasonal_factor).mean() \
                        + model.link_p[self.tfp_air.pypsa_name,snapshot] * (self.cop_air*self.seasonal_factor_air).mean() \
                        <=self.nominal_power
        
            network.model.tfpsum = Constraint(list(snapshots),rule=tfpsum)
        
        
class GeothermalHeatpump(WaterWaterHeatpump):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "GeoThermalHeatPump"

        self.set_costs_from_properties_file('heat_pump', 'geothermal_heatpump')
        
        # ---------------------------------------
        # Compute the COP based on the model from https://www.researchgate.net/publication/255759857_A_review_of_domestic_heat_pumps
        delta_temp = self.sink_bus.temperature - self.source_bus.temperature
        cop = 8.77 - 0.15*(delta_temp) + 0.000734*np.power(delta_temp, 2)

        self.cop = np.round(np.ones(8760)*cop, 1)
        
        self.compressor_nominal_power = self.nominal_power/self.cop.mean()
        print(self.compressor_nominal_power,self.nominal_power,self.cop.mean())
            
            
    def add_to_pypsa_network(self):
        logging.info("Adding " + self.name + " to pypsa")
        self.pypsa_sim.add(
            "Link",
            name = self.pypsa_name,
            bus0 = self.electricity_bus.pypsa_name,
            bus1 = self.source_bus.pypsa_name,
            bus2 = self.sink_bus.pypsa_name,
            efficiency = -(self.cop-1)*self.seasonal_factor,
            efficiency2 = self.cop*self.seasonal_factor,
            p_nom = self.compressor_nominal_power,
            p_min_pu = 0.0,
            p_max_pu = 1.0
        )
        


class DryCooler(Heatpump):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "DryCooler"

        self.set_costs_from_properties_file('heat_pump', 'dry_cooler')

        self.cop = np.round(np.ones(8760)*15, 1)

        if self.nominal_power is not None:
        	self.compressor_nominal_power = self.nominal_power/np.mean(self.cop)

        logging.info('Adding heat pump ' + self.name + ' with source bus ' + self.source_bus.name + ' and sink bus ' + self.sink_bus.name)
        self.add_to_pypsa_network()
