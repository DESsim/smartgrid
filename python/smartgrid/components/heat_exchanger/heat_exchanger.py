from ..link import Connexion, Link
import numpy as np
import logging

class HeatExchanger(Connexion):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "HeatExchanger"
        self.set_costs_from_properties_file('heat_exchanger', 'heat_exchanger')
        self.bidirectionnal = False

        if np.sum(self.source_bus.temperature < self.sink_bus.temperature) > 0.0:
            raise ValueError('The sink bus ' + self.sink_bus.pypsa_name + ' has sometimes a greater temperature (' + str(np.max(self.sink_bus.temperature)) + ' degrees)  than the source bus ' + self.source_bus.pypsa_name + ' (' + str(np.min(self.source_bus.temperature)) + ' degrees). Try replacing the heat exchanger by a heatpump, raise the source bus temperature, or remove the link and add a generator to the sink bus.')

        logging.info('Adding heat exchanger ' + self.name + ' with source bus ' + self.source_bus.name + ' and sink bus ' + self.sink_bus.name)
        
        
class MultiExchanger(Link):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "MultiExchanger"
        self.set_costs_from_properties_file('heat_exchanger', 'multi_exchanger')
        self.bidirectionnal = False
        self.source_bus2 = properties['source_bus2']
        self.seasonal_factor = properties["seasonal_factor"]


        self.add_to_pypsa_network()

    def add_to_pypsa_network(self):
        logging.info("Adding " + self.name + " to pypsa")
        
        if (self.source_bus.temperature >= self.source_bus2.temperature).all():
            print("Source_bus has higher temperature than source_bus2")
            efficiency = (np.minimum(self.source_bus.temperature, self.sink_bus.temperature) - self.source_bus2.temperature)/(self.sink_bus.temperature - self.sink_bus.return_temperature)
        elif (self.source_bus.temperature <= self.source_bus2.temperature).all():
            print("Source_bus2 has higher temperature than source_bus")            
            efficiency = (np.minimum(self.source_bus2.temperature, self.sink_bus.temperature) - self.source_bus.temperature)/(self.sink_bus.temperature - self.sink_bus.return_temperature)
        else:
            raise "The temperatures of {} and {} are not consistent".format(self.source_bus.pypsa_name,self.source_bus2.pypsa_name)
        
        print(efficiency[:15])
        efficiency = np.where(efficiency==0,0.0001,efficiency)
        self.pypsa_sim.add(
            "Link",
            name = self.pypsa_name,
            bus0 = self.source_bus.pypsa_name,
            bus1 = self.source_bus2.pypsa_name,
            bus2 = self.sink_bus.pypsa_name,
            efficiency2 = 1/efficiency*self.seasonal_factor,
            efficiency = -(1/efficiency-1)*self.seasonal_factor,
            p_min_pu = 0,
            p_max_pu = 1.0,
            p_nom = self.nominal_power
        )


    
    def extract_results(self):
        
        vector = self.vector
              
        # Source
        source_power = self.pypsa_sim.links_t.p0[self.pypsa_name].values
        self.power_inputs.append({
            'from': self.source_bus.pypsa_name,
            'to': self.pypsa_name,
            "vector": vector,
            'power': source_power
        })

        # Source2
        source_power2 = self.pypsa_sim.links_t.p1[self.pypsa_name].values
        self.power_inputs.append({
            'from': self.source_bus2.pypsa_name,
            'to': self.pypsa_name,
            "vector": vector,
            'power': source_power2
        })


        # Heat injection in the sink bus
        sink_power = -self.pypsa_sim.links_t.p2[self.pypsa_name].values
        self.power_outputs.append({
            'from': self.pypsa_name,
            'to': self.sink_bus.pypsa_name,
            "vector": vector,
            'power': sink_power
        })       


    # def __init__(self,network,properties):
    #     super().__init__(network,properties)
    #     self.make_efficiency(properties)
    # def make_efficiency(self,properties):
    #     T_dcw = properties["T_dcw"]
    #     T_dhw = properties["T_dhw"]
    #     T_preheat = LE_BUS_QUI_VA_BIEN
    #     preheat_efficiency = properties["preheat_efficiency"]

    #     self.efficiency


class DHWPreHeating(Link):
    """
    Works like a 3 virtually 3 input buses MultiExanger.
    Domestic Cold Water bus is virtual.
    Necessary entries :
    - preheat bus
    - "T_dcw" // int or np.array // Domestic cold water temperature
    - "T_dhw" // int or np.array // Domestic hot water temperature
    - "preheat_efficiency" // int or np.array // Efficiency of the preheating exchanger
    """
    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "DHWPreHeating"
        self.set_costs_from_properties_file('heat_exchanger', 'pre_heating')
        self.bidirectionnal = False
        self.preheat_bus = properties["preheat_bus"]

        try:
            self.seasonal_factor = properties["seasonal_factor"]
        except:
            self.seasonal_factor = np.ones(24*365)

        self.make_efficiency(properties)

        self.add_to_pypsa_network()

    def make_efficiency(self,properties):
        if (self.sink_bus.temperature > self.source_bus.temperature).all():
            raise "The temperatures of {} and {} are not consistent".format(self.source_bus.pypsa_name,self.sink_bus2.pypsa_name)
        if (self.preheat_bus.temperature > self.sink_bus.temperature).all():
            raise "The temperatures of {} and {} are not consistent".format(self.preheat_bus.pypsa_name,self.sink_bus2.pypsa_name)
        
        try:
            preheat_efficiency = properties["preheat_efficiency"]
        except:
            preheat_efficiency = 1

        T_dcw = properties["T_dcw"]
        T_preheat = self.preheat_bus.temperature
        T_dhw = self.sink_bus.temperature

        self.efficiency = preheat_efficiency * (T_preheat - T_dcw) / (T_dhw - T_dcw)
        print("*******",self.efficiency)


    def add_to_pypsa_network(self):
        logging.info("Adding " + self.name + " to pypsa")

        self.pypsa_sim.add(
            "Link",
            name = self.pypsa_name,
            bus0 = self.preheat_bus.pypsa_name,
            bus1 = self.source_bus.pypsa_name,
            bus2 = self.sink_bus.pypsa_name,
            efficiency2 = 1/self.efficiency*self.seasonal_factor,
            efficiency = -(1/self.efficiency-1)*self.seasonal_factor,
            p_min_pu = 0,
            p_max_pu = 1.0,
            p_nom = self.nominal_power
        )


    
    def extract_results(self):
        
        vector = self.vector
        # PREHEATING
        source_power = self.pypsa_sim.links_t.p0[self.pypsa_name].values
        self.power_inputs.append({
            'from': self.preheat_bus.pypsa_name,
            'to': self.pypsa_name,
            "vector": vector,
            'power': source_power
        })

        # MAIN SOURCE
        source_power2 = self.pypsa_sim.links_t.p1[self.pypsa_name].values
        self.power_inputs.append({
            'from': self.source_bus.pypsa_name,
            'to': self.pypsa_name,
            "vector": vector,
            'power': source_power2
        })


        # Heat injection in the sink bus
        sink_power = -self.pypsa_sim.links_t.p2[self.pypsa_name].values
        self.power_outputs.append({
            'from': self.pypsa_name,
            'to': self.sink_bus.pypsa_name,
            "vector": vector,
            'power': sink_power
        })       










