from ..generator import FixedGenerator
import logging
import numpy as np

class WindTurbine(FixedGenerator):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "WindTurbine"

        self.set_costs_from_properties_file('wind_turbine', 'wind_turbine')
        self.add_to_pypsa_network()

