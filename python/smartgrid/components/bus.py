import logging

class Bus():

    def __init__(self, network, name):
        self.network = network
        self.name = name
        self.pypsa_name = self.network.name + '.bus.' + name
        self.add_to_pypsa_network()
        logging.info('Adding bus ' + name)

    def add_to_pypsa_network(self):
        logging.info('Adding bus ' + self.pypsa_name + ' to pypsa')
        self.network.smartgrid.pypsa_sim.add("Bus", name = self.pypsa_name)

class HeatBus(Bus):

    def __init__(self, network, name, temperature, return_temperature=None, rh=None):
        """
            temperature : Temperature of the bus in celsius degrees
            temperature : RH of the bus in %
        """
        super().__init__(network, name) 
        self.temperature = temperature
        self.return_temperature = return_temperature
        self.rh = rh