from ..link import Connexion
import logging

class ElectricWaterHeater(Connexion):

	def __init__(self, network, properties):
		super().__init__(network, properties)
		self.type = "ElectricWaterHeater"        
		self.set_costs_from_properties_file('electric_water_heater', 'electric_water_heater')

