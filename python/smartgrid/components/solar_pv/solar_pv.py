from ..generator import FixedGenerator
import logging
import numpy as np

class SolarPV(FixedGenerator):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "SolarPV"

        self.set_costs_from_properties_file('solar_pv', 'solar_pv')
        self.add_to_pypsa_network()

