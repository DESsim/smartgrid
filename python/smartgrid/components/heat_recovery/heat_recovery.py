from ..heat_pump.heat_pump import Heatpump
import logging
import numpy as np

class WasteWaterHeatpump(Heatpump):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "WasteWaterHeatpump"

        # Check if the power array is a numpy array
        if isinstance(properties['power'], np.ndarray) == False:
            ValueError('Power time series should be passed as a numpy array.')
        else:
            self.waste_water_power = properties['power']

        self.cop = 4.2

        self.set_costs_from_properties_file('heat_recovery', 'heat_recovery')
        self.add_to_pypsa_network()

    def add_to_pypsa_network(self):

        logging.info("Adding " + self.pypsa_name + " to pypsa")

        # Diminish the recoverable power to account for 
        # the mixing of cold water with the hot water
        recoverable_dhw_power = self.waste_water_power*0.55

        # Account for the lag between hot water delivery and waste water evacuation
        recoverable_dhw_power = 0.5*(recoverable_dhw_power + np.roll(recoverable_dhw_power, 1))

        # Limit the recovery to 75% of the max recoverable power
        recoverable_dhw_power = np.where(recoverable_dhw_power > 0.75*recoverable_dhw_power.max(), 0.75*recoverable_dhw_power.max(), recoverable_dhw_power)

        # Compute the power output of the heatpump
        power_output = recoverable_dhw_power/(1-1/self.cop)

        # Add the waste water recoverable power to the waste water bus
        self.pypsa_sim.add(
            "Generator",
            name = self.pypsa_name + "_recoverable_waste_water",
            bus = self.source_bus.pypsa_name,
            p_min_pu = 0.0,
            p_max_pu = (recoverable_dhw_power/recoverable_dhw_power.max()).round(3),
            p_nom = recoverable_dhw_power.max()
        )

        # Add the heatpump transfering heat from the waste water bus
        # to the domestic hot water bus
        self.pypsa_sim.add(
            "Link",
            name = self.pypsa_name,
            bus0 = self.electricity_bus.pypsa_name,
            bus1 = self.source_bus.pypsa_name,
            bus2 = self.sink_bus.pypsa_name,
            efficiency = -(self.cop-1),
            efficiency2 = self.cop,
            p_min_pu = 0.0,
            p_max_pu = (power_output/power_output.max()).round(3),
            p_nom = power_output.max()/self.cop
        )

        # Add a small storage to balance supply and demand
        # since the production of the heatpump lags the hot water delivery
        self.pypsa_sim.add(
            "StorageUnit",
            name = self.pypsa_name + "_storage",
            bus = self.sink_bus.pypsa_name,
            p_nom = power_output.max(),
            max_hours = 1.0
        )


    def extract_results(self):

        # Heat extraction from the source bus
        source_power = self.pypsa_sim.links_t.p1[self.pypsa_name].values

        self.power_inputs.append({
            'vector': 'heating',
            'from': self.source_bus.pypsa_name,
            'to': self.pypsa_name,
            'power': source_power
        })

        # Electrical power of the heatpump's compressor
        compressor_power = self.pypsa_sim.links_t.p0[self.pypsa_name].values

        self.power_inputs.append({
            'vector': 'electricity',
            'from': self.electricity_bus.pypsa_name,
            'to': self.pypsa_name,
            'power': compressor_power
        })

        # Heat injection in the sink bus
        sink_power = -self.pypsa_sim.links_t.p2[self.pypsa_name].values

        self.power_outputs.append({
            'vector': 'heating',
            'from': self.pypsa_name,
            'to': self.sink_bus.pypsa_name,
            'power': sink_power
        })


        # Storage
        power = self.pypsa_sim.storage_units_t.p[self.pypsa_name + "_storage"].values
        power_charge = np.where(power < 0, -power, 0)
        power_discharge = np.where(power > 0, power, 0)

        self.power_inputs.append({
            'vector': 'heating',
            'from': self.sink_bus.pypsa_name,
            'to': self.pypsa_name + "_storage_charge",
            'power': power_charge
        })

        self.power_outputs.append({
            'vector': 'heating',
            'from': self.pypsa_name + "_storage_discharge",
            'to': self.sink_bus.pypsa_name,
            'power': power_discharge
        })


