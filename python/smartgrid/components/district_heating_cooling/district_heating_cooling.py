from ..generator import VariableGenerator, BaseGenerator
import logging
import numpy as np

class DistrictHeating(VariableGenerator):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "DistrictHeating"
        self.set_costs_from_properties_file('district_heating_cooling', 'district_heating')
        self.add_to_pypsa_network()
        
    def extract_results(self):

        power = self.round_power(self.pypsa_sim.generators_t.p[self.pypsa_name].values)

        self.power_outputs.append({
            'from': self.pypsa_name,
            'to': self.bus.pypsa_name,
            "vector": "heating",
            'power': power
        })

        if self.optimize == True:
            self.nominal_power = self.pypsa_sim.generators.p_nom_opt[self.pypsa_name]


class DistrictCooling(BaseGenerator):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "DistrictCooling"        
        self.pypsa_name = network.name + '.system.' + properties['name']
        self.pypsa_component_type = 'generator'
        self.name = properties['name']
        self.bus = properties['bus']

        if self.optimize == False:

            if "nominal_power" in properties.keys():
                self.nominal_power = properties['nominal_power']
            else:
                self.nominal_power = 1e9

        self.set_costs_from_properties_file('district_heating_cooling', 'district_cooling')
        self.add_to_pypsa_network()
    

    def add_to_pypsa_network(self):

        logging.info("Adding " + self.pypsa_name + " to pypsa")

        if self.optimize == True:

            self.pypsa_sim.add(
                "Generator",
                name = self.pypsa_name,
                bus = self.bus.pypsa_name,
                p_nom_extendable = True,
                p_nom_min = self.p_nom_min,
                p_nom_max = self.p_nom_max,
                p_min_pu = -1.0,
                p_max_pu = 0.0
            )            

        else:

            self.pypsa_sim.add(
                "Generator",
                name = self.pypsa_name,
                bus = self.bus.pypsa_name,
                p_min_pu = -1.0,
                p_max_pu = 0.0,
                p_nom = self.nominal_power
            )
            

    def extract_results(self):

        power = np.abs(self.pypsa_sim.generators_t.p[self.pypsa_name].values)

        self.power_outputs.append({
            'from': self.bus.pypsa_name,
            'to': self.pypsa_name,
            "vector": "cooling",
            'power': power
        })

        if self.optimize == True:
            self.nominal_power = self.pypsa_sim.generators.p_nom_opt[self.pypsa_name]
