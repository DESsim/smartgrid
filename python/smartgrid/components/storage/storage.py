from ..component import Component
import numpy as np
import logging

class Storage(Component):

    def __init__(self, network, properties):
        super().__init__(network)
        self.pypsa_name = network.name + '.system.' + properties['name']
        self.name = properties['name']
        self.type = "Storage"
        self.nominal_power = properties['nominal_power']
        self.max_hours = properties['max_hours']
        self.pypsa_component_type = 'storage_unit'
        self.bus = properties['bus']
        
        try:
            self.vector = properties['vector']
            if self.vector == "heating" or self.vector == "cooling" or self.vector == "electricity":
                pass
            else:
                raise ValueError('Vector must be "heating", "cooling" or "electricity"')
        except:
            raise ValueError('You must set the vector ("heating", "cooling" or "electricity") for a storage. This will not affect ground storage.')


    def compute_costs(self, metric):

        power = self.pypsa_sim.storage_units_t.p[self.pypsa_name].values

        capital_cost = self.capital_costs[metric]
        marginal_cost = self.marginal_costs[metric]

        capital_cost *= power.max()
        marginal_cost *= power

        return capital_cost, marginal_cost.sum()


    def extract_results(self):

        power = self.pypsa_sim.storage_units_t.p[self.pypsa_name].values
        power_charge = np.where(power < 0, -power, 0)
        power_discharge = np.where(power > 0, power, 0)

        self.power_inputs.append({
            'from': self.bus.pypsa_name,
            'to': self.pypsa_name,
            "vector": self.vector,
            'power': power_charge
        })

        self.power_outputs.append({
            'from': self.pypsa_name,
            'to': self.bus.pypsa_name,
            "vector": self.vector,
            'power': power_discharge
        })

        self.add_to_flowbased()

    def add_to_pypsa_network(self):

        logging.info("Adding " + self.name + " to pypsa")

        self.pypsa_sim.add(
            "StorageUnit",
            name = self.pypsa_name,
            bus = self.bus.pypsa_name,
            p_nom = self.nominal_power,
            max_hours = self.max_hours
        )

class HeatStorage(Storage):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.set_costs_from_properties_file('storage', 'water_heat_storage')
        self.add_to_pypsa_network()
        
        
class CoolStorage(Storage):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.set_costs_from_properties_file('storage', 'water_cool_storage')
        self.add_to_pypsa_network()
        
    def add_to_pypsa_network(self):

        logging.info("Adding " + self.name + " to pypsa")

        self.pypsa_sim.add(
            "StorageUnit",
            name = self.pypsa_name,
            bus = self.bus.pypsa_name,
            p_nom = self.nominal_power,
            sign = -1,
            max_hours = self.max_hours
        )


