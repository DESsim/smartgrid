from ..component import Component
import numpy as np
import logging

class Ground(Component):

    def __init__(self, network, properties):
        super().__init__(network)
        self.type = "Ground"
        self.set_costs_from_properties_file('ground', 'ground')
        self.pypsa_name = network.name + '.system.' + properties['name']
        self.name = properties['name']
        self.nominal_power = properties['nominal_power']
        self.pypsa_component_type = 'storage_unit'
        self.bus = properties['bus']
        # if "balanced" in properties.keys():
           # self.balanced = properties['balanced']
        # else:
           # self.balanced = True
        self.add_to_pypsa_network()
        
    def compute_costs(self, metric):

        power = self.pypsa_sim.storage_units_t.p[self.pypsa_name].values

        capital_cost = self.capital_costs[metric]
        marginal_cost = self.marginal_costs[metric]

        capital_cost *= power.max()
        marginal_cost *= power

        return capital_cost, marginal_cost.sum()


    def extract_results(self):

        power = self.pypsa_sim.storage_units_t.p[self.pypsa_name].values
        power_charge = np.where(power < 0, -power, 0)
        power_discharge = np.where(power > 0, power, 0)

        self.power_inputs.append({
            'from': self.bus.pypsa_name,
            'to': self.pypsa_name+'_sink',
            "vector": "cooling",
            "inSankey":True,
            'power': power_charge
        })

        self.power_outputs.append({
            'from': self.pypsa_name+"_source",
            'to': self.bus.pypsa_name,
            "vector": "heating",
            "inSankey":True,
            'power': power_discharge
        })

        self.add_to_flowbased()
    def add_to_pypsa_network(self):


        logging.info("Adding " + self.name + " to pypsa")

        self.pypsa_sim.add(
            "StorageUnit",
            name = self.pypsa_name,
            bus = self.bus.pypsa_name,
            p_nom = self.nominal_power,
            max_hours = 8760,
            state_of_charge_initial = 8760*self.nominal_power,
            cyclic_state_of_charge = False ############# Balanced?
        )




