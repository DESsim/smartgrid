from .component import Component

import numpy as np
import logging


class Link(Component):

    def __init__(self, network, properties):
        super().__init__(network)
        self.pypsa_name = network.name + '.system.' + properties['name']
        self.pypsa_component_type = 'link'
        self.type = "Link"
        self.name = properties['name']
        self.sink_bus = properties['sink_bus']
        self.source_bus = properties['source_bus']
        if "nominal_power" in properties.keys():
            self.nominal_power = properties['nominal_power']
        else:
            self.nominal_power = 1e9

        # Initialize seasonal factor
        self.seasonal_factor = properties["seasonal_factor"]
        
        try:
            self.vector = properties['vector']
            if self.vector == "heating" or self.vector == "cooling" or self.vector == "electricity" or self.vector == "fuel":
                pass
            else:
                raise ValueError('Vector must be "heating", "cooling", "electricity" or "fuel"')
        except:
            raise ValueError('You must set the vector ("heating", "cooling", "electricity" or "fuel") for a link')

    def compute_costs(self, metric):

        power = self.pypsa_sim.links_t.p0[self.pypsa_name].values

        capital_cost = self.capital_costs[metric]
        marginal_cost = self.marginal_costs[metric]

        capital_cost *= power.max()
        marginal_cost *= power

        return capital_cost, marginal_cost.sum()


class Connexion(Link):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "Connexion"
        self.bidirectionnal = False
        if "efficiency" in properties.keys():
            self.efficiency = properties['efficiency']
        else:
            self.efficiency = 1.0

        self.add_to_pypsa_network()

    def add_to_pypsa_network(self):

        logging.info("Adding " + self.name + " to pypsa")

        if self.bidirectionnal is True:
            p_min_pu = -1.0
        else:
            p_min_pu = 0.0

        self.pypsa_sim.add(
            "Link",
            name = self.pypsa_name,
            bus0 = self.source_bus.pypsa_name,
            bus1 = self.sink_bus.pypsa_name,
            p_min_pu = p_min_pu,
            p_max_pu = 1.0,
            p_nom = self.nominal_power,
            efficiency = self.efficiency*self.seasonal_factor
        )


    def extract_results(self):

        output_power = np.abs(self.pypsa_sim.links_t.p0[self.pypsa_name].values)
        
        vector = self.vector

        self.power_inputs.append({
            'from': self.source_bus.pypsa_name,
            'to': self.pypsa_name,
            "vector": vector,
            'power': output_power
        })

        self.power_outputs.append({
            'from': self.pypsa_name,
            'to': self.sink_bus.pypsa_name,
            "vector": vector,
            'power': output_power
        })

        




