from ..generator import VariableGenerator
import logging

class GasBoiler(VariableGenerator):

	def __init__(self, network, properties):
		super().__init__(network, properties)
		self.type = "GasBoiler"
		self.set_costs_from_properties_file('gas_boiler', 'condensing_gas_boiler')
		self.add_to_pypsa_network()

	def extract_results(self):

		  power = self.round_power(self.pypsa_sim.generators_t.p[self.pypsa_name].values)

		  self.power_outputs.append({
				'from': self.pypsa_name,
				'to': self.bus.pypsa_name,
				"vector": "heating",
				'power': power
		  })

		  if self.optimize == True:
		      self.nominal_power = self.pypsa_sim.generators.p_nom_opt[self.pypsa_name]