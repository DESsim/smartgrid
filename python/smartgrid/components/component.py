import logging
import numpy as np
from pathlib import Path
import pandas as pd

class Component():

    def __init__(self, network):
        self.network = network
        self.pypsa_sim = network.smartgrid.pypsa_sim
        self.flow_based = network.smartgrid.flow_based
        self.pypsa_component_type = None
        self.type = "Component"

        self.capital_costs = {
            'primary_energy': 0.0,
            'ghg_emissions': 0.0,
            'monetary': 0.0
        }

        self.marginal_costs = {
            'primary_energy': 0.0,
            'ghg_emissions': 0.0,
            'monetary': 0.0
        }

        self.set_costs_from_properties_file('', 'generic_component')

        self.power_inputs = []
        self.power_outputs = []


    def set_costs_from_properties_file(self, prop_folder, sheet_name):

        # Load the excel sheet
        path = Path(__file__).parent
        prop_folder = prop_folder
        prop_file = 'properties.xlsx'
        data = pd.read_excel(path / prop_folder / prop_file, sheet_name = sheet_name)

        # Find the header line and the fisrt line
        header_line = data.iloc[2].values
        first_line = data.iloc[7].values

        # Find the number of lines (power sgements) in the table
        N_lines = data.shape[0] - 7
        
        # Extract the tables from the excel sheet
        def load_table(table_name):

            # Lines to extract
            start_row = 7
            end_row = 7 + N_lines

            # Columns to extract
            start_column = np.where(header_line == table_name)[0][0]

            # Find the empty columns index (columns marking the tables separations)
            end_column = np.argwhere(pd.isnull(first_line))
            end_column = [index for index in end_column if index > start_column]
            if len(end_column) > 0:
                end_column = end_column[0][0]
            else:
                end_column = data.shape[1]

            # Subset the pandas df
            table = data.iloc[start_row:end_row, start_column:end_column]
            table.columns = data.iloc[start_row-2, start_column:end_column].values

            return table

        properties = load_table('Properties')
        invest = load_table('Operations & Maintenance costs')
        om = load_table('Operations & Maintenance costs')
        marginal = load_table('Marginal use cost')

        # Set the cost data of the component object
        # For now only one value per component is allowed (power segment = 'all')
        # We should allow multiple values to model the cost / power dependance
        def compute_annualized_capital_cost(cost_type):
            return invest.loc[ invest['Power'] == 'all', cost_type ]/properties.loc[ properties['Power'] == 'all', 'Lifetime' ]

        # Capital costs are annualized investment costs + O&M costs (unit : X/kW)
        self.capital_costs['primary_energy'] = compute_annualized_capital_cost('Primary energy') + om.loc[ om['Power'] == 'all', 'Primary energy' ]
        self.capital_costs['ghg_emissions'] = compute_annualized_capital_cost('GHG emissions')  + om.loc[ om['Power'] == 'all', 'GHG emissions' ]
        self.capital_costs['monetary'] = compute_annualized_capital_cost('Monetary')  + om.loc[ om['Power'] == 'all', 'Monetary' ]

        # Marginal costs (unit : X/kWh)
        self.marginal_costs['primary_energy'] = marginal.loc[ marginal['Power'] == 'all', 'Primary energy' ].values[0]
        self.marginal_costs['ghg_emissions'] = marginal.loc[ marginal['Power'] == 'all', 'GHG emissions' ].values[0]
        self.marginal_costs['monetary'] = marginal.loc[ marginal['Power'] == 'all', 'Monetary' ].values[0]



    def set_pypsa_costs(self, primary_energy_weight, ghg_emissions_weight, monetary_weight):

        capital_cost = primary_energy_weight*self.capital_costs['primary_energy']
        capital_cost += ghg_emissions_weight*self.capital_costs['ghg_emissions']
        capital_cost += monetary_weight*self.capital_costs['monetary']

        marginal_cost = primary_energy_weight*self.marginal_costs['primary_energy']
        marginal_cost += ghg_emissions_weight*self.marginal_costs['ghg_emissions']
        marginal_cost += monetary_weight*self.marginal_costs['monetary']
        pd.options.mode.chained_assignment = None 
        pypsa_components = getattr(self.network.smartgrid.pypsa_sim, self.pypsa_component_type + 's')
        pypsa_components['capital_cost'][self.pypsa_name] = capital_cost

        # Test if the marginal cost is a time series to modify pypsa
        if isinstance(marginal_cost, np.ndarray) == True or isinstance(marginal_cost, pd.Series) == True:
            pypsa_components = getattr(self.network.smartgrid.pypsa_sim, self.pypsa_component_type + 's_t')
            pypsa_components['marginal_cost'][self.pypsa_name] = marginal_cost
        else:
            pypsa_components = getattr(self.network.smartgrid.pypsa_sim, self.pypsa_component_type + 's')
            pypsa_components['marginal_cost'][self.pypsa_name] = marginal_cost
        pd.options.mode.chained_assignment = "warn" 
            
        


    def compute_costs(self, metric):
        # Default method that should be overriden for each component type (generator, link...)
        return 0.0, 0.0

    def add_to_flowbased(self):
        flows = self.power_inputs + self.power_outputs
        for flow in flows:
            self.flow_based.G.add_edge(
                flow['from'],
                flow['to'],
                p = flow['power']
            )

    def round_power(self,power):
        return np.round(power, 6)


