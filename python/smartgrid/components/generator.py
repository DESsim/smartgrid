from .component import Component
import logging
import numpy as np


class BaseGenerator(Component):

    def __init__(self, network, properties):
        super().__init__(network)
        self.pypsa_name = network.name + '.system.' + properties['name']
        self.pypsa_component_type = 'generator'
        self.type = "BaseGenerator"
        self.name = properties['name']
        self.bus = properties['bus']

        self.optimize = False
        self.p_nom_min = 0.0
        self.p_nom_max = 1e12

        if 'optimize' in properties.keys():

            self.optimize = True
            self.network.smartgrid.optimize = True

            if 'minimum_nominal_power' in properties['optimize'].keys():
                self.p_nom_min = properties['optimize']['minimum_nominal_power']

            if 'maximum_nominal_power' in properties['optimize'].keys():
                self.p_nom_max = properties['optimize']['maximum_nominal_power']


    def compute_costs(self, metric):

        power = self.pypsa_sim.generators_t.p[self.pypsa_name].values

        capital_cost = self.capital_costs[metric]
        marginal_cost = self.marginal_costs[metric]

        capital_cost *= power.max()
        marginal_cost *= power

        return capital_cost, marginal_cost.sum()

    def extract_results(self):

        power = self.round_power(self.pypsa_sim.generators_t.p[self.pypsa_name].values)

        self.power_outputs.append({
            'from': self.pypsa_name,
            'to': self.bus.pypsa_name,
            "vector": "heating",
            'power': power
        })



class VariableGenerator(BaseGenerator):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.pypsa_name = network.name + '.system.' + properties['name']
        self.pypsa_component_type = 'generator'
        self.type = "VariableGenerator"
        self.name = properties['name']
        self.bus = properties['bus']

        if self.optimize == False:

            if "nominal_power" in properties.keys():
                self.nominal_power = properties['nominal_power']
            else:
                self.nominal_power = 1e9
    

    def add_to_pypsa_network(self):

        logging.info("Adding " + self.pypsa_name + " to pypsa")

        if self.optimize == True:

            self.pypsa_sim.add(
                "Generator",
                name = self.pypsa_name,
                bus = self.bus.pypsa_name,
                p_nom_extendable = True,
                p_nom_min = self.p_nom_min,
                p_nom_max = self.p_nom_max
            )            

        else:

            self.pypsa_sim.add(
                "Generator",
                name = self.pypsa_name,
                bus = self.bus.pypsa_name,
                p_nom = self.nominal_power
            )


    def extract_results(self):

        power = self.round_power(self.pypsa_sim.generators_t.p[self.pypsa_name].values)

        self.power_outputs.append({
            'from': self.pypsa_name,
            'to': self.bus.pypsa_name,
            "vector": "electricity",
            'power': power
        })

        if self.optimize == True:
            self.nominal_power = self.pypsa_sim.generators.p_nom_opt[self.pypsa_name]



class FixedGenerator(BaseGenerator):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "FixedGenerator"
        self.positive_generator = True

        # Check if the power array is a numpy array
        if isinstance(properties['power'], np.ndarray) == False:
            ValueError('Power time series should be passed as a numpy array.')
        else:
            self.power = properties['power']

            # Check if the generator is injecting from or extracting to the bus it is connected to
            if np.sum(self.power) < 0:
                self.positive_generator = False

        if "power_smoothing" in properties.keys():
            if properties["power_smoothing"] == True:
                self.power = np.convolve(self.power, [0.125, 0.75, 0.125], 'same')


    def add_to_pypsa_network(self):

        logging.info("Adding " + self.pypsa_name + " to pypsa")

        if self.optimize == True:

            if self.positive_generator == True:

                self.pypsa_sim.add(
                    "Generator",
                    name = self.pypsa_name,
                    bus = self.bus.pypsa_name,
                    p_nom_extendable = True,
                    p_nom_min = self.p_nom_min,
                    p_nom_max = self.p_nom_max,
                    p_max_pu = (self.power/self.power.max()).round(3)
                )

            else:

                 self.pypsa_sim.add(
                    "Generator",
                    name = self.pypsa_name,
                    bus = self.bus.pypsa_name,
                    p_nom_extendable = True,
                    p_nom_min = self.p_nom_min,
                    p_nom_max = self.p_nom_max,
                    p_min_pu = -(self.power/self.power.max()).round(3),
                    p_max_pu = 0
                )

        else:

            if self.positive_generator == True:

                self.pypsa_sim.add(
                    "Generator",
                    name = self.pypsa_name,
                    bus = self.bus.pypsa_name,
                    p_nom = self.power.max(),
					p_min_pu = 0.0,
                    p_max_pu = (self.power/self.power.max()).round(3)
                )


            else:

                power = np.abs(self.power)

                self.pypsa_sim.add(
                    "Generator",
                    name = self.pypsa_name,
                    bus = self.bus.pypsa_name,
                    p_min_pu = -(power/power.max()).round(3),
                    p_max_pu = 0,
                    p_nom = power.max()
                )



    def extract_results(self):

        pypsa_power = self.round_power(self.pypsa_sim.generators_t.p[self.pypsa_name].values)

        if np.sum(np.abs(pypsa_power)) < 0.98*np.sum(np.abs(self.power)):
            logging.warning('Production from ' + self.pypsa_name + ' was curtailed, maybe because it exceeded the load on the bus it is connected to, and the surplus could not be exported to an external bus ?')

        if self.positive_generator == True:

            self.power_outputs.append({
                'from': self.pypsa_name,
                'to': self.bus.pypsa_name,
                "vector": "electricity",
                'power': pypsa_power
            })

        else:

            self.power_outputs.append({
                'from': self.bus.pypsa_name,
                'to': self.pypsa_name,    
                'power': -pypsa_power
            })







class InfiniteSource(BaseGenerator):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "InfiniteSource"
        self.add_to_pypsa_network()

    def add_to_pypsa_network(self):

        logging.info("Adding " + self.pypsa_name + " to pypsa")

        self.pypsa_sim.add(
            "Generator",
            name = self.pypsa_name,
            bus = self.bus.pypsa_name,
            p_nom = 1e9
        )


class InfiniteSink(BaseGenerator):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "InfiniteSink"
        self.add_to_pypsa_network()

    def add_to_pypsa_network(self):

        logging.info("Adding " + self.pypsa_name + " to pypsa")

        self.pypsa_sim.add(
            "Generator",
            name = self.pypsa_name,
            bus = self.bus.pypsa_name,
            p_min_pu = -1,
            p_max_pu = 0,
            p_nom = 1e9
        )

    def extract_results(self):

        power = - self.round_power(self.pypsa_sim.generators_t.p[self.pypsa_name].values)

        self.power_outputs.append({
            'from': self.bus.pypsa_name,
            'to': self.pypsa_name,
            "vector": "cooling",
            'power': power
        })


class HeatSource(FixedGenerator):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "HeatSource"
        self.add_to_pypsa_network()


class HeatSink(FixedGenerator):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "HeatSink"
        self.add_to_pypsa_network()

class AmbientAirSource(InfiniteSource):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "AmbientAirSource"



class AmbientAirSink(InfiniteSink):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "AmbientAirSink"

