from ..link import Link, Connexion
from ..generator import InfiniteSource
from ..storage.storage import Storage
import logging
import numpy as np
import pandas as pd


class FuelTank(Link):
    """
    "Macro-component" modelling a fuel tank with a compressor.
    This "macro-component" creates:
        -1 Compressor (Link) // models the electric consuption of the compressor. Inlet only.
        -1 Tank (Strorage) // models the gas bottles
        -1 Output (Connexion) // models the outlet pipe. Outlet only.
        -1 inner bus // bus dedicated to the exchanges inside the macro-component
    The compressor creates a load proportionnal to the inlet fuel flow rate.
    The outputs can be configured with "sankey-opt":
        - 'c' : includes compressor loads // default value
        - 'i' : includes fuel inlet
        - 'o' : includes fuel outlet
        written as : 'coi', 'co' ...
    """
    def __init__(self, network, properties):
        self.name = properties["name"]
        self.type = "FuelTank"
        # INTIALIZING SANKEY OPTIONS
        if "sankey-opt" in properties.keys():
            self.sankey_opt = properties["sankey-opt"]
        else:
            self.sankey_opt = "c"
        # INITIALIZING SEASONAL_FACTOR AND VECTOR
        if "seasonal_factor" in properties.keys():
            self.seasonal_factor = properties['seasonal_factor']
        else:
            self.seasonal_factor = np.ones(24*365)  
        try:
            self.vector = properties['vector']
            if self.vector == "heating" or self.vector == "cooling" or self.vector == "electricity" or self.vector == "fuel":
                pass
            else:
                raise ValueError('Vector must be "heating", "cooling", "electricity" or "fuel"')
        except:
            raise ValueError('You must set the vector ("heating", "cooling", "electricity" or "fuel") for a link')
        
        # CREATE INNER BUS
        self.make_inner_bus(network)
        # CREATE COMPRESSOR
        compressor_properties = {
            "component_type":properties["component_type"],
            "name":properties["name"]+"_compressor",
            "vector":properties["vector"],
            "source_bus":properties["source_bus"],
            "electricity_bus":properties["electricity_bus"],
            "sink_bus":self.inner_bus,
            "nominal_power":properties["nominal_power"],
            "seasonal_factor":self.seasonal_factor,
            "efficiency":properties["efficiency"]}
        super().__init__(network,compressor_properties)
        # CREATE COMPRESSOR
        self.make_compressor(network,compressor_properties)
        # CREATE TANK
        self.make_tank(network,properties)
        # CREATE OUTPUT CONNEXION
        self.make_output(network,properties)

    def make_inner_bus(self,network):
        inner_bus_name = "inner_bus_" + self.name
        network.add_buses([{"name": inner_bus_name}])
        self.inner_bus = network.buses[inner_bus_name]

    def make_compressor(self,network,compressor_properties):
        self.name = compressor_properties["name"]
        self.pypsa_name = network.name + '.system.' + self.name
        self.source_bus = compressor_properties["source_bus"]
        self.electricity_bus = compressor_properties["electricity_bus"] # Electricity bus
        self.sink_bus = compressor_properties["sink_bus"]        # Heat bus
        self.conso_ratio = -(1-compressor_properties["efficiency"]) * self.seasonal_factor # Electrical consumption
        self.p_nom = compressor_properties["nominal_power"]
        self.add_to_pypsa_network()

    def add_to_pypsa_network(self):
        logging.info("Adding " + self.name + " to pypsa")

        self.pypsa_sim.add(
            "Link",
            name = self.pypsa_name,
            bus0 = self.source_bus.pypsa_name,      # H2 bus,
            bus1 = self.electricity_bus.pypsa_name, # Electricity bus
            bus2 = self.sink_bus.pypsa_name,        # Inner bus
            efficiency = self.conso_ratio, # Electrical consumption ratio
            efficiency2 = 1, 
            p_min_pu = 0,
            p_max_pu = 1.0,
            p_nom = self.p_nom
        )

        # if self.nominal_power is not None:
        #     self.pypsa_sim.links.loc[self.pypsa_name, "p_nom"] = self.absorbed_power

    def make_tank(self,network,properties):
        # CREATE STORAGE
        tank_properties = {
            "name": properties['name']+"_tank",
            'nominal_power': properties['nominal_power'],
            'max_hours': properties['max_hours'],
            "bus":self.inner_bus,
            "vector":properties["vector"]}
        self.tank_name = tank_properties["name"]
        self.tank = Storage(network,tank_properties)
        self.tank.set_costs_from_properties_file('fuel_cell', 'FT_tank')
        self.tank.add_to_pypsa_network()
        self.tank.extract_results = self.extract_null
        network.components[tank_properties["name"]] = self.tank

    def make_output(self,network,properties):
        """ Dedicated Connexion modelling the output pipe.
        Does not consume electric energy
        Erasing the extract_results method as the inner bus must not appear on the sankey
        """
        output_properties = {
            "name":self.name+"_output",
            "vector":properties["vector"],
            "sink_bus":properties["source_bus"],
            "source_bus":self.inner_bus,
            "nominal_power":properties["nominal_power"],
            "seasonal_factor":self.seasonal_factor}

        self.output_name = output_properties["name"]
        self.output = Connexion(network,output_properties)
        self.output.extract_results = self.extract_null
        network.components[output_properties["name"]] = self.output
    
    def extract_null(self):
        1

    def extract_results(self):
        # Electrical consumption
        print(self.pypsa_name)
        if "c" in self.sankey_opt:
            compressor_power = self.pypsa_sim.links_t.p1[self.pypsa_name].values
            self.power_inputs.append({
                'from': self.electricity_bus.pypsa_name,
                'to': self.pypsa_name,
                "vector" : "electricity",
                'power': compressor_power
            })
        # Fuel flows
        #   Fuel inlet
        if "i" in self.sankey_opt:
            fuel_input = self.pypsa_sim.links_t.p0[self.pypsa_name].values
            self.power_inputs.append({
                'from': self.source_bus.pypsa_name,
                'to': self.tank.pypsa_name,
                "vector": 'fuel',
                'power': fuel_input
            })
        #   Fuel outlet
        if "o" in self.sankey_opt:
            self.output.pypsa_name
            fuel_outlet = - self.pypsa_sim.links_t.p1[self.output.pypsa_name].values
            self.power_outputs.append({
                'from': self.tank.pypsa_name,
                'to': self.source_bus.pypsa_name,
                "vector": 'fuel',
                'power': fuel_outlet
            })

class FuelSource(InfiniteSource):
    """
    Generic fuel source.
    Fuel can be: CH4, H2... 
    """
    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "FuelSource"
        self.set_costs_from_properties_file('fuel_cell', 'generic_fuel_source')
        
    def extract_results(self):
        power = self.round_power(self.pypsa_sim.generators_t.p[self.pypsa_name].values)

        self.power_outputs.append({
            'from': self.pypsa_name,
            'to': self.bus.pypsa_name,
            "vector": "fuel", 
            'power': power
        })

class FuelCell(Link):
    """
    Actual Efficiency depends on the load rate. At low load, efficiency is about 53+%. At highet loads, 45%.
    Due to not working directly with variable efficiencies, an option is integrated to export the load of the fuel cell, and compute an average or hourly efficiency
    bus0 = fuel                // INPUT
    bus1 = electrical power    // OUTPUT
    bus2 = sink / heating      // OUTPUT
    Typical efficiencies : Fuel -> Power 40-45% // General 95%
    """
    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "FuelCell"
        # GENERIC ASSIGNMENTS
        self.electricity_bus = properties['electricity_bus']
        self.elec_efficiency = properties['elec_efficiency']
        general_efficiency = properties['general_efficiency']
        self.heat_efficiency = general_efficiency - self.elec_efficiency
        # EFFICIENCY CHECKS
        if general_efficiency > 1:
            logging.warning("General efficiency of {} is too high (>1)".format(self.pypsa_name))
        if self.heat_efficiency <0:
            raise ValueError("Electrical efficiency of {} is superior than general efficiency".format(self.pypsa_name))
        if self.elec_efficiency == 0:
            raise ValueError("Electrical efficiency of {} is null".format(self.pypsa_name))
        # EXPORT CHECK
        try:
            if properties['export_to'] != '' :
                self.export_load_rate = True
                self.path = properties['export_to']
            else:
                self.export_load_rate = False
                self.path=''
        except:
            self.export_load_rate = False
            self.path=''
        # POWER ASSIGNMENTS
        electric_nominal_power = properties['nominal_power']
        self.absorbed_power = electric_nominal_power / self.elec_efficiency
        # ADDING TO PYPSA
        self.add_to_pypsa_network()

    def add_to_pypsa_network(self):
        logging.info("Adding " + self.name + " to pypsa")

        self.pypsa_sim.add(
            "Link",
            name = self.pypsa_name,
            bus0 = self.source_bus.pypsa_name,      # H2 bus,
            bus1 = self.electricity_bus.pypsa_name, # Electricity bus
            bus2 = self.sink_bus.pypsa_name,        # Heat bus
            efficiency = + self.elec_efficiency * self.seasonal_factor, # Electrical production efficiency
            efficiency2 = + self.heat_efficiency * self.seasonal_factor,  # Heat recovery efficiency
            p_min_pu = 0,
            p_max_pu = 1.0
        )

        if self.nominal_power is not None:
            self.pypsa_sim.links.loc[self.pypsa_name, "p_nom"] = self.absorbed_power

    def save_load_rate(self):
        load_rate = self.pypsa_sim.links_t.p0[self.pypsa_name].values / self.absorbed_power
        file_name = "{}_load_rate.csv".format(self.source_bus.pypsa_name)
        load_rate = pd.DataFrame(load_rate)
        try:
            load_rate.to_csv(self.path + file_name)
        except:
            print('{} not exported'.format(file_name))

    def extract_results(self):
        # H2 consumption
        fuel_power = self.pypsa_sim.links_t.p0[self.pypsa_name].values
        self.power_inputs.append({
            'from': self.source_bus.pypsa_name,
            'to': self.pypsa_name,
            "vector": 'fuel',
            'power': fuel_power
        })
        # Electrical production
        electric_power = - self.pypsa_sim.links_t.p1[self.pypsa_name].values
        self.power_outputs.append({
            'from': self.pypsa_name,
            'to': self.electricity_bus.pypsa_name,
            "vector" : "electricity",
            'power': electric_power
        })
        # Heat injection in the sink bus
        sink_power = - self.pypsa_sim.links_t.p2[self.pypsa_name].values
        self.power_outputs.append({
            'from': self.pypsa_name,
            'to': self.sink_bus.pypsa_name,
            "vector": 'heating',
            'power': sink_power
        })

        if self.export_load_rate:
            self.save_load_rate()

class Electrolyser(Connexion):
    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.set_costs_from_properties_file('fuel_cell', 'electrolyser')
        self.bidirectionnal = False
        logging.info('Adding electrolyser ' + self.name + ' with source bus ' + self.source_bus.name + ' and sink bus ' + self.sink_bus.name)

class MethaneReformer(Link):
    """
    Methane reformer. Transforms CH4 into H2.
    Can be powered by electricity or CH4
    'source_bus': CH4
    'sink_bus': H2
    'in_power_bus': electricity
    'efficiency': 0.73 -> See 
    PRODUCTION D’HYDROGENE A PARTIR DES PROCEDES DE REFORMAGE ET D’OXYDATION PARTIELLE // Mémento de l'Hydrogène // Fiche 3.1, AFHYPAC
    https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=4&ved=2ahUKEwiV3fS4_I3oAhVLxhoKHX6tAgIQFjADegQIBBAB&url=http%3A%2F%2Fbellevue-albi.entmip.fr%2FlectureFichiergw.do%3FID_FICHIER%3D1338642551903&usg=AOvVaw37Dk3VSSLuRxWYImz29fv4

    """

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "MethaneReformer"

        self.in_power_bus = properties["source_bus2"]
        
        if "efficiency" in properties.keys():
            self.efficiency = properties['efficiency']
        else:
            self.efficiency = 0.73

        # self.elec_consumption = 0.01
        self.elec_consumption = 0.0

        self.add_to_pypsa_network()

    def add_to_pypsa_network(self):

        logging.info("Adding " + self.name + " to pypsa")

        self.pypsa_sim.add(
            "Link",
            name = self.pypsa_name,
            bus0 = self.source_bus.pypsa_name,
            bus1 = self.sink_bus.pypsa_name,
            bus2 = self.in_power_bus.pypsa_name,
            p_min_pu = 0,
            p_max_pu = 1.0,
            p_nom = self.nominal_power,
            efficiency = self.efficiency * self.seasonal_factor,
            efficiency2 = - self.elec_consumption * self.seasonal_factor
        )


    def extract_results(self):
        print("*********************",self.pypsa_name)
        vector = self.vector

        # CH4 consumption
        fuel_power = self.pypsa_sim.links_t.p0[self.pypsa_name].values
        self.power_inputs.append({
            'from': self.source_bus.pypsa_name,
            'to': self.pypsa_name,
            "vector": vector,
            'power': fuel_power
        })
        
        h2_power = - self.pypsa_sim.links_t.p1[self.pypsa_name].values
        self.power_outputs.append({
            'from': self.pypsa_name,
            'to': self.sink_bus.pypsa_name,
            "vector" : "fuel",
            'power': h2_power
        })

        in_power_power = self.pypsa_sim.links_t.p2[self.pypsa_name].values
        self.power_inputs.append({
            'from': self.in_power_bus.pypsa_name,
            'to': self.pypsa_name,
            "vector": 'electricity',
            'power': in_power_power
        })
