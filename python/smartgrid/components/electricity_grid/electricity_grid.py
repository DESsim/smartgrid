from ..generator import InfiniteSource, InfiniteSink

class ElectricitySupply(InfiniteSource):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "ElectricitySupply"        
        self.set_costs_from_properties_file('electricity_grid', 'electricity_supply')
        
    def extract_results(self):

        power = self.round_power(self.pypsa_sim.generators_t.p[self.pypsa_name].values)

        self.power_outputs.append({
            'from': self.pypsa_name,
            'to': self.bus.pypsa_name,
            "vector": "electricity",
            'power': power
        })
        
        
    
class ElectricityExport(InfiniteSink):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.type = "ElectricityExport"        
        self.set_costs_from_properties_file('electricity_grid', 'electricity_export')
        
    def extract_results(self):

        power = - self.round_power(self.pypsa_sim.generators_t.p[self.pypsa_name].values)

        self.power_outputs.append({
            'from': self.bus.pypsa_name,
            'to': self.pypsa_name,
            "vector": "electricity",
            'power': power
        })
