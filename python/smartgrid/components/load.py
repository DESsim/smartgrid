from .component import Component
import numpy as np
import logging

class Load(Component):

    def __init__(self, network, properties):
        super().__init__(network)
        self.pypsa_name = network.name + '.load.' + properties['name']
        self.type = 'load'
        self.pypsa_component_type = 'load'
        self.name = properties['name']
        self.bus = properties['bus']
        self.power = properties['power']

        # Check if the power array is a numpy array
        if isinstance(properties['power'], np.ndarray) == False:
            ValueError('Power time series should be passed as a numpy array.')

        if "power_smoothing" in properties.keys():
            if properties["power_smoothing"] == True:
                self.power = np.convolve(self.power, [0.125, 0.75, 0.125], 'same')

        # Check if the power array has any non zero values
        energy = np.abs(np.sum(self.power))
        if energy > 0:
            self.zero_load = False
        else:
            logging.warning(self.pypsa_name + ' has zero power consumption : it will not be added to pypsa and its power inputs will be directly set to zero in the results.')
            self.zero_load = True

        self.add_to_pypsa_network()

    def add_to_pypsa_network(self):

        logging.info("Adding " + self.pypsa_name + " to pypsa")

        if self.zero_load == False:

            self.pypsa_sim.add(
                "Load",
                name = self.pypsa_name,
                bus = self.bus.pypsa_name,
                p_set = self.power
            )

    def set_pypsa_costs(self, primary_energy_weight, ghg_emissions_weight, monetary_weight):
        # Load components have no capital or marginal costs
        # so we override the set_cost method of the parent class component
        pass



class ElectricityLoad(Load):

    def __init__(self, network, properties):
        super().__init__(network, properties)

    def extract_results(self):

        if self.zero_load == False:

            power = self.pypsa_sim.loads_t.p[self.pypsa_name].values

            self.power_inputs.append({
                'from': self.bus.pypsa_name,
                'to': self.pypsa_name,
                "vector": "electricity",
                'power': np.abs(power)
            })

        else:

            self.power_inputs.append({
                'from': self.bus.pypsa_name,
                'to': self.pypsa_name,
                "vector": "electricity",
                'power': np.zeros(8760)
            })


class HeatingLoad(ElectricityLoad):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.temperature = properties['temperature']
        # Check if the temperatures of the load and the bus are compatible
        if np.sum(self.bus.temperature < self.temperature) > 0:
            raise ValueError('The temperature of the load ' + self.name + ' is superior to the temperature of the bus ' + self.bus.name + ' it is connected to.')

    def extract_results(self):

        if self.zero_load == False:

            power = self.pypsa_sim.loads_t.p[self.pypsa_name].values

            self.power_inputs.append({
                'from': self.bus.pypsa_name,
                'to': self.pypsa_name,
                "vector": "heating",
                'power': np.abs(power)
            })

        else:

            self.power_inputs.append({
                'from': self.bus.pypsa_name,
                'to': self.pypsa_name,
                "vector": "heating",
                'power': np.zeros(8760)
            })

class CoolingLoad(Load):

    def __init__(self, network, properties):
        super().__init__(network, properties)
        self.temperature = properties['temperature']

        # Check if the temperatures of the load and the bus are compatible
        if np.sum(self.bus.temperature > self.temperature) > 0:
            raise ValueError('The temperature of the load ' + self.name + ' is inferior to the temperature of the bus ' + self.bus.name + ' it is connected to.')

    def extract_results(self):

        if self.zero_load == False:

            power = self.pypsa_sim.loads_t.p[self.pypsa_name].values

            self.power_inputs.append({
                'from': self.pypsa_name,
                'to': self.bus.pypsa_name,
                "vector": "cooling",
                'power': np.abs(power)
            })

        else:

            self.power_inputs.append({
                'from': self.pypsa_name,
                'to': self.bus.pypsa_name,
                "vector": "cooling",
                'power': np.zeros(8760)
            })
