import networkx as nx
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from functools import reduce

class FlowBased:
    
    def __init__(self):
        self.G = nx.DiGraph()
        self.n_snapshots = 0
        
    def setup(self):
        
        np.seterr(divide='ignore', invalid='ignore')
        
        # Create inflow and outflow time series for each edge
        for u, v, p in self.G.edges.data('p'):
            self.G.edges[u, v]['p_pos'] = np.where(p > 0, p, 0)
            self.G.edges[u, v]['p_neg'] = np.where(p < 0, -p, 0)
        
        # Compute the inflow and outflow mix of each node
        for node in self.G.nodes:
    
            p_in_tot = 0
            p_out_tot = 0
            self.G.nodes[node]['flows'] = {}
            
            # Identify nodes at the edge of the graph
            # (with no parents nor children)
            if len(list(self.G.successors(node))) < 1 or len(list(self.G.predecessors(node))) < 1:
                self.G.nodes[node]['leaf'] = True
            else:
                self.G.nodes[node]['leaf'] = False
            
            # Identify source nodes
            if len(list(self.G.predecessors(node))) < 1:
                self.G.nodes[node]['is_source'] = True
                self.G.nodes[node]['is_sink'] = False
            elif len(list(self.G.successors(node))) < 1:
                self.G.nodes[node]['is_source'] = False
                self.G.nodes[node]['is_sink'] = True
            else:
                self.G.nodes[node]['is_source'] = False
                self.G.nodes[node]['is_sink'] = False

            # Compute the total inflow and outflow of the node
            for successor in self.G.successors(node):
                p_out_tot += self.G.edges[node, successor]['p_pos']
                p_in_tot += self.G.edges[node, successor]['p_neg'] 

            for predecessor in self.G.predecessors(node):
                p_in_tot += self.G.edges[predecessor, node]['p_pos']
                p_out_tot += self.G.edges[predecessor, node]['p_neg']
            
            # Store the total input and output flows
            self.G.nodes[node]['p_in_tot'] = p_in_tot
            self.G.nodes[node]['p_out_tot'] = p_out_tot

            # Compute the share of each inflow or outflow
            # in the total inflow or outflow of the node
            
            # Negative flow for an edge with a children = inflow
            # Positive flow for an edge with a children = outflow
            for successor in self.G.successors(node):
                self.G.nodes[node]['flows'][successor] = {}
                self.G.nodes[node]['flows'][successor]['source'] = self.G.edges[node, successor]['p_neg']/p_in_tot
                self.G.nodes[node]['flows'][successor]['sink'] = self.G.edges[node, successor]['p_pos']/p_out_tot
            
            # Negative flow for an edge with a parent = outflow
            # Positive flow for an edge with a parent = inflow
            for predecessor in self.G.predecessors(node):
                self.G.nodes[node]['flows'][predecessor] = {}
                self.G.nodes[node]['flows'][predecessor]['source'] = self.G.edges[predecessor, node]['p_pos']/p_in_tot
                self.G.nodes[node]['flows'][predecessor]['sink'] = self.G.edges[predecessor, node]['p_neg']/p_out_tot
                
        # Store the number of snapshots
        self.n_snapshots = list(self.G.edges.values())[0]['p'].shape[0]
            
        np.seterr(divide='warn', invalid='warn')
                
    def collect_flows(self, node, direction, i):

        if direction == 'source':
            sep = '<'
        else:
            sep = '>'
        
        # Walk the graph to collect all flows connected to the node
        # Share the source or sink flow at each node 
        def step_upstream(node, path, k):

            for n, v in self.G.nodes[node]['flows'].items():
                kn = v[direction][i]
                if kn > 0:
                    flows.append([node, path + sep + n, k*kn, self.G.nodes[n]['leaf']])
                    step_upstream(n, path + sep + n, k*kn)
        
        flows = []
        step_upstream(node, node, 1.0)
        
        # Keep only leaf nodes
        flows = [f for f in flows if f[3] is True]

        return flows
    
    
    def get_mix(self, node, direction, relative=True, metric='final_energy'):

        nodes = {}
        
        # At each timestamp, find the final source or sink flows of a node
        for i in range(self.n_snapshots):

            flows = self.collect_flows(node, direction, i)

            for f in flows:
                if f[1] not in nodes.keys():
                    nodes[f[1]] = []
                nodes[f[1]].append([i, f[2]])

        # Create a timestamp / share dataframe for each source or sink
        dfs = []
        for n, data in nodes.items():

            df = pd.DataFrame(data)
            df.columns = ['time', n]

            ref = pd.DataFrame({'time': [i for i in range(8760)]})
            df = pd.merge(ref, df, on = 'time', how = 'left')
            df.fillna(value=0, inplace=True)

            df = df.groupby('time', as_index=False)[n].sum()

            dfs.append(df)

        if len(dfs) < 1:
            df = pd.DataFrame({'time': [i for i in range(8760)]})
            df['none'] = 0
            dfs.append(df)


        # Merge all dataframes to get the source or sink mix of the node
        df = reduce(lambda x, y: pd.merge(x, y, on = 'time'), dfs)

        # If absolute power mix values are queried (relative=False) 
        # Multiply by the input or output total flow of the node
        if relative is False:

            power = pd.DataFrame({'time': [i for i in range(8760)]})

            if direction == 'source':
                power['power'] = self.G.nodes[node]['p_in_tot']
            else:
                power['power'] = self.G.nodes[node]['p_out_tot']

            df = pd.melt(df, 'time')
            df = pd.merge(df, power, on = 'time')
            df['power'] = df['power']*df['value']

            df = df.pivot(index='time', columns='variable', values='power')
            df.reset_index(inplace=True)

        return df


        