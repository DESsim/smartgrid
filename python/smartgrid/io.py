import pickle
import bz2
import pypsa
from smartgrid import *
from .flowbased import FlowBased
import numpy as np
from zipfile import ZipFile
import os

def load_network(path):
    
    with ZipFile(path, 'r') as zip_ref:
        zip_ref.extractall(os.path.dirname(path))
    
    pickle_path = path[:-3]+"p"
    nc_path = path[:-3]+"nc"
    with bz2.BZ2File(pickle_path, 'r') as file:
        obj = pickle.load(file)

    obj.pypsa_sim = smartgrid.init_pypsa()
    obj.pypsa_sim.import_from_netcdf(nc_path)
    
    # Reinitialize links to pypsa objects
    obj.flow_based = FlowBased()
    for n, network in obj.networks.items():
        for c, component in network.components.items():
            component.pypsa_sim = obj.pypsa_sim
            component.flow_based = obj.flow_based
    obj.extract_results()
    
    os.remove(pickle_path)
    os.remove(nc_path)


    return obj


