import logging
# logging.basicConfig(level=logging.ERROR)

import pypsa
pypsa.opf.logger.setLevel(logging.ERROR)
pypsa.pf.logger.setLevel(logging.ERROR)

import numpy as np
import pandas as pd
import plotly.graph_objects as go

import pickle
import bz2
import copy
import os
from zipfile import ZipFile
import yaml
import re

from .network import Network
from .flowbased import FlowBased

class Smartgrid():

    def __init__(self):

        self.name = 'Smartgrid'
        self.networks = {}
        self.optimize = False

        pypsa_sim = init_pypsa()
        pypsa_sim.set_snapshots(range(8760))

        self.pypsa_sim = pypsa_sim
        self.flow_based = FlowBased()
        
        self.summary = pd.DataFrame()

    def add_network(self, name):
        self.networks[name] = Network(self, name)
        return self.networks[name]


    def add_networks_from_config(self, config_file_path, variables):

        # Load the configuration yaml
        with open(config_file_path) as file:
            text = file.read()
            config = yaml.full_load(text)

        # Create the networks
        for n, params in config["networks"].items():
            
            network_variables = variables[n]
            network_variables.update(variables["global"])
            
            # Load and eval the variables defined in the config file
            if "variables" in params.keys():
                eval_var = {}
                for k, v in params["variables"].items():
                    if isinstance(v, str):
                        if "<" in v:
                            v = v.replace("<", "")
                            v = v.replace(">", "")
                            eval_var[k] = eval(v, network_variables)
                        else:
                            eval_var[k] = v
                    else:
                        eval_var[k] = v

                network_variables.update(eval_var)

            # Load and fill in the template
            self.add_network_from_template(
                network_name=n,
                yaml_file_path="./sampledata/"+params["template"],
                variables=network_variables
            )


    def add_network_from_template(self, network_name, yaml_file_path, variables=None):

        def eval_inputs(inputs, variables):
            eval_inp = []
            for inp in inputs:

                # Search for list variables (between << >>)
                list_var = False
                for p, par in inp.items():
                    if isinstance(par, str):
                        if "<<" in par:
                            list_var = True
                            cases_var = re.search("<<.*>>", par).group(0)
                            cases_var = cases_var.replace("<<", "").replace(">>", "")
                            break

                # If no list variable is present
                # we can just loop through the variables and evaluate them
                if list_var == False:

                    for p, par in inp.items():
                        if isinstance(par, str):

                            # Evaluate the parameters between < > by their value
                            if "<" in par:
                                par = par.replace("<", "").replace(">", "")
                                inp[p] = eval(par, variables)

                            else:
                                inp[p] = par

                        else:
                            inp[p] = par

                    eval_inp.append(inp)

                # If there is a list variable, we have to duplicate the inputs as many time as there is cases,
                # loop through each case and evaluate variables while changing the environment, to match the values of that case
                else:
                    eval_inp_list = []

                    for case in variables[cases_var]:

                        # Create a separate environment for the variables
                        variables_subcomp = variables.copy()
                        inp_subcomp = inp.copy()

                        # In this envionement, replace the variables containing a dict (one value for each case)
                        # by the value for the case being evaluated
                        for k, var in variables_subcomp.items():
                            if k != "__builtins__" and isinstance(var, dict): # Bug : builtins dict appearing in the variables dict... 
                                variables_subcomp[k] = variables_subcomp[k][case]

                        for p, par in inp.items():
                            if isinstance(par, str):

                                # Evaluate the parameters between < > by their value
                                if "<" in par and "<<" not in par:
                                    par = par.replace("<", "").replace(">", "")
                                    inp_subcomp[p] = eval(par, variables_subcomp)

                                elif "<<" in par:
                                    par = re.sub("<<.*>>", case, par)
                                    par = par.replace("<<", "").replace(">>", "")
                                    inp_subcomp[p] = par

                                else:
                                    inp_subcomp[p] = par

                            else:
                                inp_subcomp[p] = par

                        eval_inp_list.append(inp_subcomp)

                    eval_inp += eval_inp_list

            return eval_inp
                        
        
        # Load the yaml file
        with open(yaml_file_path) as file:
            text = file.read()
            networks = yaml.full_load(text)
        
        # For each network defined in the yaml file
        for n, net in networks.items():
            
            # Create a network in the smartgrid object
            if n == "network":
                network = self.add_network(network_name)
                variables.update({"network": network})

            # Create the bus objects in the smartgrid object
            buses = eval_inputs(net["buses"], variables)
            network.add_buses(buses)
            
            components = eval_inputs(net["components"], variables)
            network.add_components(components)
    
    def getConstraints(self):
        constraints = []
        for network in self.networks:
            constraints.extend(self.networks[network].constraints)
        ############ WORKS WITH ONLY ONE CONSTRAINT FOR NOW
        if len(constraints)  == 1:
            extra_functionality = constraints[0]
        else:
            extra_functionality = None
        return extra_functionality   

        

    def solve(self, primary_energy_weight=0.0, ghg_emissions_weight=0.0, monetary_weight=1.0, split_by_month=False, solver="cbc",extra_functionality=None):

        for network_name, network in self.networks.items():
            network.set_pypsa_costs(primary_energy_weight, ghg_emissions_weight, monetary_weight)

        # Set logging to error when solving the model
        # to avoid flooding the console with PyPSA logs
        logging.basicConfig(level=logging.ERROR)
        


        # The model is solved by default for the whole year
        # If any component is being optimized we also run the model for the whole year
        # to avoid finding different optimal sizings for each month
        if self.optimize == True or split_by_month == False:

            self.pypsa_sim.lopf(
                solver_name=solver,
                formulation='kirchhoff',
                extra_functionality=self.getConstraints()
            )



        # If no component is being optimized or the user considers it OK,
        # we can split the problem by month
        else:
            for m in range(12):
                print("Month {}/12".format(m+1))
                self.pypsa_sim.lopf(
                    solver_name=solver,
                    formulation='kirchhoff',
                    snapshots = range(m*730, (m+1)*730),
                    extra_functionality=self.getConstraints()
                )


        # Reset logging once the model is solved
        logging.basicConfig(level=logging.INFO) 

        self.extract_results()


    def extract_results(self):
        for network_name, network in self.networks.items():
            network.extract_results()

        self.flow_based.setup()


    def compute_costs(self, metric):

        total_capital_cost = 0.0
        total_marginal_cost = 0.0

        # Compute the costs of each component
        for network_name, network in self.networks.items():
            capital_cost, marginal_cost = network.compute_costs(metric)
            total_marginal_cost += marginal_cost
            total_capital_cost += capital_cost

        return total_capital_cost + total_marginal_cost



    def normalize_optimization_weights(self):

        # Monetary optimum
        self.solve(
            primary_energy_weight=0.0,
            ghg_emissions_weight=0.0,
            monetary_weight=1.0
        )

        monetary_optimum = {}
        monetary_optimum['monetary'] = self.compute_costs('monetary')
        monetary_optimum['primary_energy'] = self.compute_costs('primary_energy')
        monetary_optimum['ghg_emissions'] = self.compute_costs('ghg_emissions')

        # GHG emissions optimum
        self.solve(
            primary_energy_weight=0.0,
            ghg_emissions_weight=1.0,
            monetary_weight=0.0
        )

        ghg_emissions_optimum = {}
        ghg_emissions_optimum['monetary'] = self.compute_costs('monetary')
        ghg_emissions_optimum['primary_energy'] = self.compute_costs('primary_energy')
        ghg_emissions_optimum['ghg_emissions'] = self.compute_costs('ghg_emissions')

        # Find the scaling factor
        monetary_scale = max(ghg_emissions_optimum['monetary'], monetary_optimum['monetary']) - min(ghg_emissions_optimum['monetary'], monetary_optimum['monetary'])
        ghg_emissions_scale = max(ghg_emissions_optimum['ghg_emissions'], monetary_optimum['ghg_emissions']) - min(ghg_emissions_optimum['ghg_emissions'], monetary_optimum['ghg_emissions'])

        if monetary_scale < 1e-3:
            raise ValueError('No monetary cost difference between optimal solutions. Make sure there are components that can be changed to optimize the cost (gas boiler replacing a district heating connexion for example).')
        elif ghg_emissions_scale < 1e-3:
            raise ValueError('No GHG emissions difference between optimal solutions. Make sure there are components that can be changed to optimize the emissions (heatpump replacing a gas boiler for example).')
        else:
            ghg_scaling_factor = monetary_scale/ghg_emissions_scale

        return ghg_scaling_factor

        


    def sankey_plot(self, img_path, width, height):

        # Prepare edge data to match plotly format
        # Dataframe of indexed edges
        if len(self.summary) == 0:
            self.create_summary()
        edges = self.summary.rename({"energy":"volume"},axis=1).drop(["max_power","load_factor"],axis=1)
        edges = edges[edges.inSankey == True]
        edges = edges.loc[edges['from'] != edges['to']]
        nodes = edges['from'].unique().tolist() + edges['to'].unique().tolist()
        nodes = list(set(nodes))
        edges_index = pd.DataFrame({'from': nodes, 'to': nodes})
        edges_index['from_index'] = np.arange(0, edges_index.shape[0])
        edges_index['to_index'] = np.arange(0, edges_index.shape[0])

        edges = pd.merge(edges, edges_index[['from', 'from_index']], on = 'from')
        edges = pd.merge(edges, edges_index[['to', 'to_index']], on = 'to')

        colors = pd.DataFrame({'vector': ['electricity', 'heating', 'hot_water', 'cooling','fuel'], 'color': ['#f1c40f', '#e74c3c', '#c0392b', '#3498db','#d2ff4d']})

        colors['rgba_0.25'] = colors['color'].apply(lambda x: 'rgba(' + ','.join([str(int(x.lstrip('#')[i:i+2], 16)) for i in (0, 2, 4)]) + ',0.5)')
        colors['rgba_0.75'] = colors['color'].apply(lambda x: 'rgba(' + ','.join([str(int(x.lstrip('#')[i:i+2], 16)) for i in (0, 2, 4)]) + ',0.75)')
        edges = pd.merge(edges, colors, on = 'vector')

        fig = go.Figure(data=[go.Sankey(
            node = dict(
              pad = 15,
              thickness = 10,
              line = dict(color = "white", width = 0.5),
              label = edges_index['from'],
              color = '#34495e'
            ),
            link = dict(
              source = edges['from_index'],
              target = edges['to_index'],
              value = edges['volume'],
              color = edges['rgba_0.25']
          ))])

        fig.update_layout(font_size=10, width = width, height = height)

        fig.write_image(img_path)
        return edges

    def remove_results(self):
        for network_name, network in self.networks.items():
            for c, component in network.components.items():
                self.networks[network_name].components[c].power_inputs = []
                self.networks[network_name].components[c].power_outputs = []
                self.networks[network_name].components[c].flow_based = None
                self.networks[network_name].components[c].pypsa_sim = None
                self.networks[network_name].components[c].pypsa_component_type = None
                # self.networks[network_name].components[c].capital_costs = None
                # self.networks[network_name].components[c].marginal_costs = None
                
                    
        self.pypsa_sim = None
        self.flow_based = None

                            
    def save(self, path):
        obj = copy.deepcopy(self)
        print("Saving Pypsa networks")
        obj.pypsa_sim.export_to_netcdf("{}.nc".format(path))
        obj.remove_results()
        print("Pickling networks")
        with bz2.BZ2File("{}.p".format(path), 'w') as picklefile:
            pickle.dump(obj,picklefile)
        print("Zipping smartgrid to {}.zip".format(path))
        with ZipFile("{}.zip".format(path), 'w') as zipObj:
           zipObj.write("{}.nc".format(path),"{}.nc".format(os.path.basename(path)))
           zipObj.write("{}.p".format(path),"{}.p".format(os.path.basename(path)))
        os.remove("{}.p".format(path))
        os.remove("{}.nc".format(path))
    
    def create_summary(self):
        def max_power(x):
            return np.max(np.abs(x))
        dfs = []
        for network_name, network in self.networks.items():
            for component_name, component in network.components.items():
                for flow in component.power_inputs + component.power_outputs:
                    df = pd.DataFrame.from_dict(flow)
                    if "inSankey" not in df.columns:
                        df["inSankey"] = True
                    df = df.groupby(['from', 'to', "vector", "inSankey"], as_index=False).agg({'power': [np.sum, max_power]})
                    dfs.append(df)
        dfs = pd.concat(dfs)
        dfs.columns = ['from', 'to', "vector", "inSankey", 'energy', 'max_power']
        dfs['load_factor'] = dfs['energy']/dfs['max_power']/8760
        self.summary = dfs
    
    def create_detailed_export(self):
        def max_power(x):
            return np.max(np.abs(x))
        dfs = []
        for network_name, network in self.networks.items():
            for component_name, component in network.components.items():
                for flow in component.power_inputs + component.power_outputs:
                    df = pd.DataFrame.from_dict(flow)
                    # df = df.groupby(['from', 'to', "vector"], as_index=False).agg({'power': [np.sum, max_power]})
                    df["time"] = np.arange(0,8760,1)
                    dfs.append(df)
        dfs = pd.concat(dfs)
        dfs.columns = ['from', 'to', "vector", 'max_power', 'time']
        # dfs['load_factor'] = dfs['energy']/dfs['max_power']/8760
        self.detailed_export = dfs
    
    def save_detailed_export(self, path):
        self.create_detailed_export()
        dfs = self.detailed_export
        dfs.to_excel(os.path.join(path,'detailed_export.xlsx'),index=False)
        print("Saved {} in {}".format("detailed export",path))

    def save_summary(self, path):
        self.create_summary()
        dfs = self.summary
        dfs = dfs.drop("inSankey",axis=1)
        dfs.to_excel(os.path.join(path,'summary.xlsx'),index=False)
        print("Saved {} in {}".format("summary",path))
        
    def get_fluxes(self):
        fluxes = pd.DataFrame()
        for network_name in self.networks.keys():
            for component_name in self.networks[network_name].components.keys():
                component = self.networks[network_name].components[component_name]
                for output in component.power_outputs:
                        fluxes[("out",output["vector"],network_name,component.type,component_name)] = output["power"]
                for inpt in component.power_inputs:
                        fluxes[("in",inpt["vector"],network_name,component.type,component_name)] = inpt["power"]
        fluxes.columns = pd.MultiIndex.from_tuples(fluxes.columns)
        fluxes.columns.rename(["direction","vector","network","type","name"],inplace=True)
        return fluxes
        
    def plot(self,path):
        pass
        # import matplotlib.pyplot as plt
        # heating = pd.DataFrame()
        # cooling = pd.DataFrame()
        # heatcool = pd.DataFrame()
        # my_smartgrid = self
        # print("Plotting...")
        # for cname in my_smartgrid.networks["network"].components.keys():
        #     ctype = str(type(my_smartgrid.networks["network"].components[cname]))
        #     if ("heat_pump" in ctype or "District" in ctype or "Storage" in ctype or "Gas" in ctype) and not "Adiabatic" in ctype:
        #         output = my_smartgrid.networks["network"].components[cname].power_outputs[0]
        #         if output["vector"] == "heating":
        #             heating[output["from"].replace("network.system.","")] = output["power"]
        #         if output["vector"] == "cooling":
        #             cooling[output["from"].replace("network.system.","")] = output["power"]
        #         heatcool[output["from"].replace("network.system.","")] = output["power"]
                    
        # heating.index = pd.date_range(freq="h", start="2005-01-01", periods=365*24)
        # cooling.index = pd.date_range(freq="h", start="2005-01-01", periods=365*24)
        # heatcool.index = pd.date_range(freq="h", start="2005-01-01", periods=365*24)
        
        # def monthlyplot(yearly,name,path):
        #     monthly = yearly.resample("M").sum()
        #     monthly.index = ["J","F","M","A","M","J","J","A","S","O","N","D"]
        #     monthly = monthly.loc[:,(monthly!=0).any(axis=0)] #remove all zeros columns
        #     monthly /= 1000 #to MWh
        #     monthly.plot(kind="bar",stacked=True)
        #     plt.ylabel('MWh')
        #     plt.savefig(os.path.join(path,name+".pdf"),bbox_inches='tight')
        
        # monthlyplot(heating,"heating",path)
        # monthlyplot(cooling,"cooling",path)
        
        # summer = heatcool[(heatcool.index>="2005-06-21")&(heatcool.index<"2005-06-28")]
        # summer = summer.loc[:,(summer!=0).any(axis=0)]
        # winter = heatcool[(heatcool.index>="2005-01-01")&(heatcool.index<"2005-01-07")]
        # winter = winter.loc[:,(winter!=0).any(axis=0)]
        # summer.plot()
        # plt.ylabel('kW')
        # plt.savefig(os.path.join(path,"summer_week.pdf"),bbox_inches='tight')
        # winter.plot()
        # plt.ylabel('kW')
        # plt.savefig(os.path.join(path,"winter_week.pdf"),bbox_inches='tight')
        

def init_pypsa():
    # Modify pyPSA link component to allow 3 bus systems (heatpumps)
    override_component_attrs = pypsa.descriptors.Dict({k : v.copy() for k,v in pypsa.components.component_attrs.items()})
    override_component_attrs["Link"].loc["bus2"] = ["string",np.nan,np.nan,"2nd bus","Input (optional)"]
    override_component_attrs["Link"].loc["efficiency2"] = ["static or series","per unit",1.,"2nd bus efficiency","Input (optional)"]
    override_component_attrs["Link"].loc["p2"] = ["series","MW",0.,"2nd bus output","Output"]
    obj = pypsa.Network(override_component_attrs=override_component_attrs)
    return obj
