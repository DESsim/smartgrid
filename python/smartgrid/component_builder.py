from .components import *
import numpy as np

class ComponentBuilder():
        
    def build(self, network, properties):

        properties['seasonal_factor'] = np.ones(8760)

        available_generator_components = {
            "gas_boiler": GasBoiler,
            "solar_pv": SolarPV,
            "wind_turbine": WindTurbine,
            "infinite_source": InfiniteSource,
            "infinite_sink": InfiniteSink,
            "heat_source": HeatSource,
            "heat_sink":  HeatSink,
            "ambient_air_source": AmbientAirSource,
            "ambient_air_sink":  AmbientAirSink,
            "solar_pv": SolarPV,
            "district_heating": DistrictHeating,
            "district_cooling": DistrictCooling,
            "electricity_supply": ElectricitySupply,
            "electricity_export": ElectricityExport,
            "generic_fuel_source":FuelSource
    
        }

        available_link_components = {
            "connexion": Connexion,
            "heat_exchanger": HeatExchanger,
            "multi_exchanger": MultiExchanger,
            "water_water_heat_pump": WaterWaterHeatpump,
            "air_water_heat_pump": AirWaterHeatpump,
            "thermorefrigerating_pump": Thermorefrigeratingpump,
            "thermorefrigerating_pump_with_air": ThermorefrigeratingpumpWithAir,
            "geothermal_heat_pump": GeothermalHeatpump,
            "electric_water_heater": ElectricWaterHeater,
            "waste_water_heat_pump": WasteWaterHeatpump,
            "adiabatic_cooler": AdiabaticCooler,
            "dry_cooler": DryCooler,
            "fuel_cell":FuelCell,
            "electrolyser":Electrolyser,
            "dhw_preheat":DHWPreHeating,
            "reformer":MethaneReformer

        }

        available_storage_components = {
            "water_heat_storage": HeatStorage,
            "water_cool_storage": CoolStorage,
            "fuel_storage":FuelTank,
            "ground":Ground
        }

        available_components = {}
        available_components.update(available_generator_components)
        available_components.update(available_link_components)
        available_components.update(available_storage_components)
        
        # ------------------
        # System builders
        if properties['component_type'] == 'system':
            
            if "season" in properties.keys():
                if properties['season'] == "winter":
                    properties['seasonal_factor'] = np.concatenate([np.ones(105*24),np.zeros(183*24),np.ones(77*24)])
                elif properties['season'] == "summer":
                    properties['seasonal_factor'] = np.concatenate([np.zeros(105*24),np.ones(183*24),np.zeros(77*24)])
                    
            if properties['system'] in available_components.keys():
                return available_components[properties['system']](network, properties)

            else:
                raise ValueError(
                    'System '+ properties['system'] + " unavailable (should be one of {}".format(", ".join(available_components.keys()))
                )


        # ------------------
        # Load builder
        elif properties['component_type'] == 'load':

            if properties['load_type'] in ['heating', 'hot_water']:
                return HeatingLoad(network, properties)
            elif properties['load_type'] == 'cooling':
                return CoolingLoad(network, properties)
            elif properties['load_type'] == 'electricity':
                return ElectricityLoad(network, properties)
            else:
                raise ValueError('Load type'+ properties['load_type'] + ' heating, cooling, hot_water, electricity)')


        # -----------------
        # Raise an error for all other component_type values
        else :

            raise ValueError('component_type should be on of : system, load')


    