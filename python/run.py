import numpy as np
import json
import pandas as pd
import os
import sys
import logging
import getopt
logging.basicConfig(level=logging.ERROR)

# Find the path of the script
script_path = os.path.abspath(__file__)
sys.path.append(os.path.dirname(script_path))

# Constants
distribution_losses = 0.05


# args
# --phasing 'S:/ECT_LC/Elioth/50_Bibliotheque/51 - Documentation/01 - _EE/32_smartgrid/testing/phasage/1/phasage.xlsx'
# --network_geometry 'S:/ECT_LC/Elioth/50_Bibliotheque/51 - Documentation/01 - _EE/32_smartgrid/testing/reseau/geometrie/out/a801dec1-32d7-11ea-8989-04ea56a22c94/base/0000-TRA_base.json'
# --systems 'S:/ECT_LC/Elioth/50_Bibliotheque/51 - Documentation/01 - _EE/32_smartgrid/testing/production/systems/in/1.csv'
# --power 'S:/ECT_LC/Elioth/50_Bibliotheque/51 - Documentation/01 - _EE/32_smartgrid/testing/consommation/besoins/climelioth/out/564fa68a-32c8-11ea-933c-04ea56a22c94/climelioth.parquet'
# --weather 'S:/ECT_LC/Elioth/50_Bibliotheque/51 - Documentation/01 - _EE/32_smartgrid/testing/meteo/out/H1a/H1a.csv'
# --solar_pv_production 'S:/ECT_LC/Elioth/50_Bibliotheque/51 - Documentation/01 - _EE/32_smartgrid/testing/production/solaire_pv/out/e4e0d51a-32e3-11ea-b7e1-04ea56a22c94/solar.csv'
# --run_years all
# --output_file_path test.json

# power_file_path = r"\\ter3ficw2k01\DATA\ECT_LC\Elioth\30_Prospection\32 - Concours et offres\BAPK24001 - HUGPN - APHP\04 - Concours\02 - Production\Calculs\energie\consommation\besoins\climelioth\out\besoins_fournis\climelioth.parquet"
# phasing_file_path = r"\\ter3ficw2k01\DATA\ECT_LC\Elioth\30_Prospection\32 - Concours et offres\BAPK24001 - HUGPN - APHP\04 - Concours\02 - Production\Calculs\energie\phasage\1\phasage.xlsx"
# network_geometry_path = r"\\ter3ficw2k01\DATA\ECT_LC\Elioth\30_Prospection\32 - Concours et offres\BAPK24001 - HUGPN - APHP\04 - Concours\02 - Production\Calculs\energie\reseau\geometrie\out\none\base\none.json"
# systems_file_path = r"\\ter3ficw2k01\DATA\ECT_LC\Elioth\30_Prospection\32 - Concours et offres\BAPK24001 - HUGPN - APHP\04 - Concours\02 - Production\Calculs\energie\production\systems\in\1.csv"
# output_folder_path = "/"
# solar_pv_path = ""
# weather_file_path = r"S:\ECT_LC\Elioth\30_Prospection\32 - Concours et offres\BAPK24001 - HUGPN - APHP\04 - Concours\02 - Production\Calculs\energie\meteo\out\H1a\H1a.csv"
# run_years = "all"
# weights = {"ghg": 1, "energy": 0, "monetary": 0}


# Load inputs
def main(argv):
    from smartgrid import losses
    # Parse arguments    
    power_file_path = ''
    phasing_file_path = ''
    solar_pv_path = ''
    wind_power_path = ''
    network_temperatures = ''
    network_geometry_path = ''
    systems_file_path = ''
    temp_needs_path = ""
    run_years = ''
    weights = {"ghg": 1, "energy": 0, "monetary": 0} #default carbon
    costs = ''
    output = ''
    solver = 'cbc'
    split_by_month = True
    try:
        opts, args = getopt.getopt(
           argv,
           '',
           ["power=", "programme=", "phasing=",
           "solar_pv_production=", "wind_power=", "run_years=",
           "weather=", "network_temperatures=", "temp_needs=", "network_geometry=", "systems=", "weight_ghg=", "weight_energy=", "weight_monetary=", "costs=", "split_by_month=", "output_file_path=","solver="]
           )
        
    except getopt.GetoptError:
        print("Check parameters")
        sys.exit(2)

  
    for opt, arg in opts:
        if opt in ("--power"):
            power_file_path = arg
        elif opt in ("--phasing"):
            phasing_file_path = arg
        elif opt in ("--solar_pv_production"):
            solar_pv_path = arg
        elif opt in ("--wind_power"):
            wind_power_path = arg
        elif opt in ("--weather"):
            weather_file_path = arg
        elif opt in ("--network_geometry"): # name of the geometry (e.g. 0000-TRA_base)
            network_geometry_path = arg
        elif opt in ("--systems"):
            systems_file_path = arg
        elif opt in ("--temp_needs"):
            temp_needs_path = arg
        elif opt in ("--weight_energy"):
            weights["energy"] = float(arg)
        elif opt in ("--weight_ghg"):
            weights["ghg"] = float(arg)
        elif opt in ("--weight_monetary"):
            weights["monetary"] = float(arg)
        elif opt in ("--costs"):
            costs_file = arg
        elif opt in ("--run_years"):
            run_years = arg
        elif opt in ("--solver"):
            solver = arg
        elif opt in ("--split_by_month"):
            print(opt,arg)
            if opt == "True":
                split_by_month = True
            print(split_by_month)
        elif opt in ("--output_file_path"):
            output_file_path = arg
            output_folder_path = os.path.dirname(arg)
         
         
    # Load files
    if network_geometry_path != "":
        with open(network_geometry_path) as f:
                network_properties = json.loads(f.read())["properties"]
        try:
            import geopandas as gpd
            network_geometry = gpd.read_file(network_geometry_path)
        except:
            print("Proceeding without network geometry")
            network_geometry = pd.DataFrame()
    else:
        network_geometry = pd.DataFrame()

    systemsdf = pd.read_csv(systems_file_path)
    systemsdf["component_type"] = "system"
    
    #Look for custom marginal costs
    if os.path.isfile(costs_file):
        costs = pd.read_csv(costs_file,header=[0,1])
    
    phasing = pd.read_excel(phasing_file_path, sheet_name = 'phasage')
    
    # Load power loads data
    try:
        with open(power_file_path, "rb") as f:
            loads_df = pd.read_parquet(f)
    except:
        try:
            loads_df = pd.read_csv(power_file_path)
        except:
                print("Unrecognized file format")
    loads_df = loads_df.groupby(["building_id","timebase"]).sum().reset_index()

    # Load weather data
    dateparse = lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S') #Use the date parser to make sure the format is recognized
    weather = pd.read_csv(weather_file_path,comment="#",index_col=[0],parse_dates=[0],date_parser=dateparse)

    #Model for Ground temperature
    if "ground_temperature" not in weather.columns:
        logging.log(msg="Computing ground temperature",level=0)
        ground_temperature = losses.compute_theta_g(weather,network_properties["depth"],network_properties["ground_type"])
        # Add ground temperature to weather
        weather.loc[:,"ground_temperature"] = ground_temperature + 273.15
                 
    
    pipesdata = pd.read_csv(os.path.join(os.path.dirname(os.path.dirname(script_path)), 'data', 'pipes.csv'))
    
    if solar_pv_path != "":
        pvdata = pd.read_csv(solar_pv_path,comment="#",index_col=[0],parse_dates=["time"],date_parser=dateparse)
    else:
        pvdata = []

    if wind_power_path != "":
        windpower = pd.read_csv(wind_power_path,index_col=[0])
    else:
        windpower = []

    # Get buildings
    bldgs = pd.DataFrame(index=loads_df.building_id.unique())
    bldgs["connected"] = False #initialize
    if len(network_geometry) > 0:
        origins = network_geometry[(network_geometry.Source=="chaud") & (network_geometry.Origin!= "0")].Origin.unique().tolist()
        destinations = network_geometry[(network_geometry.Source=="chaud") & (network_geometry.Destination!= "0")].Destination.unique().tolist()
        
        bldgs.loc[bldgs.index.isin(destinations), "connected"] = True
        bldgs.loc[bldgs.index.isin(origins), "connected"] = True
    else:
        bldgs["connected"] = True
    

    years = phasing['opening_year'].unique()

    if run_years != 'all':
        if run_years == 'last':
            years = [np.max(years)]
        else:
            years = [int(y) for y in run_years.split("-")]
            
    output_json = {}

    # # Parallel runs
    # pool = mp.Pool(4)
    # jobs = [pool.apply_async(solve_year, args=(year, network_geometry, systems, bldgs, weather, loads_df, pvdata, phasing, output_folder_path)) for year in years]
    # results = [job.get(timeout=60*10) for job in jobs]
    # pool.close()
    
    results = [] 
    for year in years:
        result = solve_year(year, weights, network_geometry, network_properties, pipesdata, systemsdf, bldgs, weather, loads_df, temp_needs_path, pvdata, windpower, phasing, costs, split_by_month, output_folder_path, solver)
        results.append(result)

    # Record the path of the result files to access them in the output json file
    for i in range(len(results)):
        output_json[str(results[i][0])] = results[i][1]
            
    # Write the output file (dict of paths pointing to the results for each year)
    with open(output_file_path, 'w') as file:
        json.dump(output_json, file)
        

def solve_year(year, weights, network_geometry, network_properties, pipesdata, systemsdf, bldgs, weather, loads_df, temp_needs_path, pvdata, windpower, phasing, costs, split_by_month, output_folder_path,solver):
        from smartgrid import smartgrid,build_network,io,losses
        print("Solving for year " + str(year))
    
        # Get all buildings that exist in year y
        open_buildings = phasing.loc[phasing['opening_year'] <= year, 'building_id'].tolist()
        

        bldgs = bldgs[bldgs.index.isin(open_buildings)]
    
        # --------------------------------------------
        # INITIALIZE NETWORK

        my_smartgrid = smartgrid.Smartgrid()
        
        #Evaluate expressions
        def evalDF(systems):
            # def parse_obj(obj_key,obj,obj_name):
            #     if type(obj_key) == str:
            #         bus, bustype = obj_key.split(".")
            #         if bus == obj_name:
            #             obj = getattr(obj,"buses")[bustype]
            #     else:
            #         obj = obj_key
            #     print("-",obj)
            #     return obj
            
        
            for bus in ["bus","source_bus","source_bus2","sink_bus","electricity_bus"]:
                systems.loc[:,bus] = systems[bus].apply(lambda x: getattr(network,"buses")[x.split(".")[1]] if (type(x) == str and x.split(".")[0] == "network") else x)
                if "environment.ambient_air_sink" or "environment.ambient_air_source" in systems.level.to_list():
                    systems.loc[:,bus] = systems[bus].apply(lambda x: getattr(energy_center,"buses")[x.split(".")[1]] if (type(x) == str and x.split(".")[0] == "energy_center") else x)
                systems.loc[:,bus] = systems[bus].apply(lambda x: getattr(building,"buses")[x.split(".")[1]] if (type(x) == str and x.split(".")[0] == "building") else x)
                systems.loc[:,bus] = systems[bus].apply(lambda x: getattr(environment,"buses")[x.split(".")[1]] if (type(x) == str and x.split(".")[0] == "environment") else x)
                if "adiabatic_cooler" in systems.system.to_list():
                    systems.loc[:,bus] = systems[bus].apply(lambda x: getattr(adiabatic,"buses")[x.split(".")[1]] if (type(x) == str and x.split(".")[0] == "adiabatic") else x)            
            return systems


        # --------------------------------------------
        # Environment
        environment = my_smartgrid.add_network('environment')
        
   
        systems = []
        #Air
        if "environment.ambient_air_source" in systemsdf.source_bus.to_list():
            environment.add_buses([
            {
              'name': 'ambient_air_source',
              'temperature': (weather.air_temperature.reset_index(drop=True) - 273.15).values
              # 'rh': (weather.relative_humidity.reset_index(drop=True)).values
            }
            ])
            logging.log(msg="Added ambient_air_source",level=0)
            systems.append(
                    {
                      'component_type': 'system', 
                      'name': 'ambient_air_source',
                      'system': 'ambient_air_source',
                      'bus': environment.buses['ambient_air_source']
                    }
                )
        if "environment.ambient_air_sink" in systemsdf.sink_bus.to_list():
            environment.add_buses([
            {
              'name': 'ambient_air_sink',
              'temperature': (weather.air_temperature.reset_index(drop=True) - 273.15).values
              # 'rh': (weather.relative_humidity.reset_index(drop=True)).values
            }
            ])
            logging.log(msg="Added ambient_air_sink",level=0)
            systems.append(
                {
                  'component_type': 'system', 
                  'name': 'ambient_air_sink',
                  'system': 'ambient_air_sink',
                  'bus': environment.buses['ambient_air_sink']
                }
            )
            
        # Ground 
        if "environment.ground" in systemsdf.source_bus.to_list():
            environment.add_buses([
            {
              'name': 'ground',
              'temperature': (weather.ground_temperature.reset_index(drop=True) - 273.15).values
            }
            ])
        
            logging.log(msg="Adding ground",level=0)
            
            systems.append(
                {
                  'component_type': 'system', 
                  'name': 'ground',
                  'system': 'ground',
                  'bus': environment.buses['ground'],
                  'nominal_power': 1e5
                }
            )

        
        environment.add_components(systems)
        
        # ---------------------------------------------        
        #Adiabatic loop
        
        if "adiabatic_cooler" in systemsdf.system.to_list():
            logging.log(msg="Adding adiabatic",level=0)
            adiabatic = my_smartgrid.add_network('adiabatic')
    
            # Simple model from Simone's Excel worksheet
            Teau = np.ones(8760)*25
            Tair = (weather.air_temperature.reset_index(drop=True) - 273.15).values
            Teau[Tair<23] = Tair[Tair<23] + 2
            
            buses = [{'name': 'cooling', 'temperature': Teau}]
            
            adiabatic.add_buses(buses)
    
        # ---------------------------------------------
        # Network
        
        network = my_smartgrid.add_network('network')
        
        
        # Initialize buses
        buses = [{'name': 'electricity'}] #electricity is always present
        

        #Renewable Energy Sources bus
        # if len(pvdata) > 0 or len(windpower) > 0:
        buses.append({'name': 'RES'})
        
        for netname in network_properties["networks"].keys():
            if "medium-temperature" in network_properties["networks"].keys():
                buses.append(
                  {'name': 'medium', 'temperature': np.concatenate([network_properties["networks"]["medium-temperature"]["winter"]["Tin"]*np.ones(105*24),
                                                                        network_properties["networks"]["medium-temperature"]["summer"]["Tin"]*np.ones(183*24),
                                                                        network_properties["networks"]["medium-temperature"]["winter"]["Tin"]*np.ones(77*24)])})
                
            else:
                buses.append({'name': netname,
                              'temperature': network_properties["networks"][netname]["Tin"]*np.ones(8760),
                              'return_temperature': network_properties["networks"][netname]["Tout"]*np.ones(8760)})
                
        #H2
        if "network.fuel" in systemsdf.source_bus.to_list():
            buses.append(
            {
              'name': 'fuel'            }
            )
        
        #Methane
        if "network.methane" in systemsdf.source_bus.to_list():
            buses.append(
            {
              'name': 'methane'            }
            )        
     

        #Geobus
        if "geothermal_heat_pump" in systemsdf.system.to_list():
            # Ground
            buses.append(
            {
              'name': 'geo',
              'temperature': (weather.ground_temperature.reset_index(drop=True) - 273.15).values}
            )
        
        network.add_buses(buses)
     

        # Initialize systems 
        extsystems = systemsdf.loc[systemsdf.level == "network",:]
    
        #Convert to dict
        extsystems = evalDF(extsystems)
        extsystems = [ v.dropna().to_dict() for k,v in extsystems.iterrows() ]

        #H2
        if "network.fuel" in systemsdf.source_bus.to_list():
            extsystems.append(
                {
                  'component_type': 'system', 
                  'name': 'h2source',
                  'system': 'generic_fuel_source',
                  'bus': network.buses['fuel']
                }
            )
    
    
        network.add_components(extsystems)

        # -----------------------------------------------
        # Energy center (optional)
        if "energy.center" in systemsdf.level.to_list():
            energy_center = my_smartgrid.add_network('energy_center')
            
            buses = [
            {'name': 'cooling', 'temperature': network_properties["networks"]["cooling"]["Tin"]*np.ones(8760)},
            {'name': 'heating', 'temperature': network_properties["networks"]["heating"]["Tin"]*np.ones(8760)},
             {'name': 'electricity'}
            ]
            
            energy_center.add_buses(buses)
            
            
            loopsystems = systemsdf.loc[systemsdf.level == "energy_center",:]
            
            
            #Convert to dict
            loopsystems = evalDF(loopsystems)
            loopsystems = [ v.dropna().to_dict() for k,v in loopsystems.iterrows() ]
            
            
            energy_center.add_components(loopsystems)
            
        # ------------------------------------
        # Losses
        if len(network_geometry) > 0:
            # Changing DN to the closest to those in the losses database
            network_geometry.loc[:,"DN"] = network_geometry.apply(lambda x: pipesdata.loc[(np.abs(pipesdata.DN-x.DN)).idxmin()],axis=1)
            # Merge pipes geometry with losses database
            network_geometry = pd.merge(network_geometry,pipesdata,on="DN")
                 
            
            Tfluids = pd.DataFrame(index=weather.index)
                 
            if "medium-temperature" in list(network_properties["networks"].keys()):
                    Tfluids.loc[:,"temperee"] = (network_properties["networks"]["medium-temperature"]["winter"]["Tin"]+network_properties["networks"]["medium-temperature"]["winter"]["Tout"])/2 # winter
                    Tfluids.loc[(Tfluids.index > "15-04-2005 00:00") & (Tfluids.index < "15-10-2005 00:00"), "temperee"] = (network_properties["networks"]["medium-temperature"]["summer"]["Tin"]+network_properties["networks"]["medium-temperature"]["summer"]["Tout"])/2 #summer
            else:
                    Tfluids.loc[:,"chaud"] = (network_properties["networks"]["heating"]["Tin"]+network_properties["networks"]["heating"]["Tout"])/2
                    Tfluids.loc[:,"froid"] = (network_properties["networks"]["cooling"]["Tin"]+network_properties["networks"]["cooling"]["Tout"])/2
            
            # get losses
            network_geometry, alllosses = losses.linearLosses(network_geometry,weather,Tfluids)
            losses_df = pd.DataFrame(index=alllosses.index)
                
            if "medium-temperature" in list(network_properties["networks"].keys()):
                    losses_df.loc[:,"Mixed"] = alllosses.loc[:,network_geometry[network_geometry.Source == "temperee"].index].sum(axis=1)
            else:
                    losses_df.loc[:,"Cool"] = alllosses.loc[:,network_geometry[network_geometry.Source == "froid"].index].sum(axis=1)
                    losses_df.loc[:,"Heat"] = alllosses.loc[:,network_geometry[network_geometry.Source == "chaud"].index].sum(axis=1)
            
            
            losses = [
                {'component_type': 'load', 'name': 'cooling_losses', "load_type" : "cooling", 'bus': network.buses['cooling'], 'power': -np.round(losses_df.Cool.values, 1), "temperature": network_properties["networks"]["cooling"]["Tin"]*np.ones(8760)},
                {'component_type': 'load', 'name': 'heating_losses', "load_type" : "heating", 'bus': network.buses['heating'], 'power': np.round(losses_df.Heat.values, 1), "temperature": network_properties["networks"]["heating"]["Tin"]*np.ones(8760)}
                ]
            
                    
            network.add_components(losses)
        
        # ------------------------------------
        # External PV
        # Check if there is external PV production
        if len(pvdata) > 0:
            for bldgid in pvdata.building_id.unique():
                if bldgid not in bldgs.index.tolist():
                    extPV = [{'component_type': 'system',
                              "name": "{}".format(bldgid),
                              "system": "solar_pv",
                              'bus': network.buses['RES'],
                              "power" : pvdata[pvdata.building_id == bldgid].groupby("time").sum().p_pv.values}]
                    network.add_components(extPV)
                    
        # ------------------------------------
        # Wind Power
        # Check if there is wind power
        if len(windpower) > 0:
            wind = [{'component_type': 'system',
                              "name": "wind_turbine",
                              "system": "wind_turbine",
                              'bus': network.buses['RES'],
                              "power" : windpower["power"].values}]
            network.add_components(wind)       
        
        # ------------------------------------
        

        for bldgid, bldg in bldgs.iterrows():
            
                logging.log(msg="Add building {}".format(bldgid),level=0)
            
                building = my_smartgrid.add_network(bldgid)
                
             
                buses = [
                {'name': 'electricity'},
                {'name': 'wastewater', 'temperature': 15*np.ones(8760)},
                ]
                
                for bus in network_properties["buildings"].keys():
                    buses.append({'name': bus, 'temperature': network_properties["buildings"][bus]["Tin"]*np.ones(8760)})

                building.add_buses(buses)
                
                
                #PV
                if len(pvdata) > 0:
                    if bldgid in pvdata.building_id.unique().tolist():
                        PV = [{'component_type': 'system',
                                      "name": "PV",
                                      "system": "solar_pv",
                                      'bus': building.buses['electricity'],
                                      "power" : pvdata[pvdata.building_id == bldgid].groupby("time").sum().p_pv.values}]
                    
                    
                        building.add_components(PV)
        
                
               
                #Get Loads
                bldgloads = loads_df.loc[loads_df.building_id == bldgid,:]
                
                def prepare_needs(needs,distribution_losses=distribution_losses):
                    needs = np.convolve(needs, [0.125, 0.75, 0.125], 'same')
                    needs = np.round(needs/1000) #to KW
                    needs = needs/(1-distribution_losses)
                    return needs
                    
                
                electricity = bldgloads.p_vent.abs() + bldgloads.p_light.abs() + bldgloads.p_equi.abs()
                electricity = prepare_needs(electricity)

                loads = [
                    {'component_type': 'load', 'name': 'electricity', "load_type" : "electricity", 'bus': building.buses['electricity'], 'power': electricity}
                    ]
        

                if len(temp_needs_path) == 0: #case without additional networks/specific loads
                    heating = bldgloads.p_heat.abs() - bldgloads.p_heat_reco.abs()
                    heating = prepare_needs(heating)

                    dhw = bldgloads.p_hw.abs()
                    dhw = prepare_needs(dhw)

                    cooling = bldgloads.p_cool.abs() - bldgloads.p_cool_reco.abs() + bldgloads.p_cool_process.abs()
                    cooling = prepare_needs(cooling)
                    
                    loads.extend([
                        {'component_type': 'load', 'name': 'cooling', "load_type" : "cooling", 'bus': building.buses['cooling'], 'power': -cooling, "temperature": network_properties["buildings"]["cooling"]["Tin"]*np.ones(8760)},
                        {'component_type': 'load', 'name': 'heating', "load_type" : "heating", 'bus': building.buses['heating'], 'power': heating, "temperature": network_properties["buildings"]["heating"]["Tin"]*np.ones(8760)},
                        {'component_type': 'load', 'name': 'dhw', "load_type" : "hot_water", 'bus': building.buses['dhw'], 'power': dhw, "temperature": network_properties["buildings"]["dhw"]["Tin"]*np.ones(8760)},
                        ])

                    
                else: #there are specific load ratios assigned to a specific network
                    #Load the CSV (one per building or the default one)
                    try:
                        temp_needs = pd.read_csv(os.path.join(temp_needs_path,bldgid+".csv"),comment="#",index_col=0,header=[0,1,2])
                    except:
                        temp_needs = pd.read_csv(os.path.join(temp_needs_path,"default.csv"),comment="#",index_col=0,header=[0,1,2])
                    
                    needs = temp_needs.columns.get_level_values(0).unique().to_list() #names of needs (Climelioth columns)
                    header = temp_needs.columns.to_frame(index=False) #header of the CSV file, used to retrieve the load_type
                    temp_needs.columns = temp_needs.columns.droplevel(2) #remove the load type (level 2)
                    split_needs = temp_needs.copy() #in this DF, we will add the splitted needs
                    split_needs.loc[:,:] = 0 #initialize to 0
                    for need in needs:
                        for net in temp_needs[need].columns.to_list():
                            split_needs.loc[:,(need,net)] += bldgloads[need].abs().values * temp_needs[need][net].values
                    # sum the needs belonging to the the same network         
                    split_needs = split_needs.groupby(level=1,axis=1).sum()
                    
                    for net in split_needs.columns:
                            load_type = header.loc[header.Network == net,"Load type"].unique()[0]
                            if load_type == "cooling":
                                factor = -1
                            else:
                                factor = 1
                            loads.append({'component_type': 'load', 'name': net,
                                          "load_type" : load_type, 'bus': building.buses[net],
                                          'power': prepare_needs(split_needs[net].values)*factor,
                                          "temperature": network_properties["buildings"][net]["Tin"]*np.ones(8760)})

                building.add_components(loads)
                
                if bldgid in systemsdf.case.tolist():
                    print("{} is a special case".format(bldgid))
                    bldgsystems = systemsdf.loc[systemsdf.case == bldgid,:]
                    bldgsystems = evalDF(bldgsystems)
                else:
                    if bldg.connected:
                        print("{} is connected".format(bldgid))
                        bldgsystems = systemsdf.loc[(systemsdf.level == "building") & (systemsdf.case == "in"),:]
                        bldgsystems = evalDF(bldgsystems)
                    else: 
                        print("{} is not connected".format(bldgid))
                        bldgsystems = systemsdf.loc[(systemsdf.level == "building") & (systemsdf.case == "out"),:]
                        bldgsystems = evalDF(bldgsystems)
                        
                bldgsystems = [ v.dropna().to_dict() for k,v in bldgsystems.iterrows() ]
                
                 #Add Heat Recovery
                for bldgsystem in bldgsystems:
                    if bldgsystem["system"] == "waste_water_heat_pump":
                        bldgsystem["power"] = building.components["dhw"].power
                   
                building.add_components(bldgsystems)


        # Set custom costs
        if len(costs) > 0:
            print("Setting custom marginal costs...")
            for netname in my_smartgrid.networks.keys():
                for column in costs.columns:
                    component = column[0]
                    cost = column[1]
                    values = costs[component][cost].dropna().values
                    if len(values) == 1: #Case of a fixed marginal cost
                        values = values[0]
                    try:
                        my_smartgrid.networks[netname].components[component].marginal_costs[cost] = values
                        print("Custom '{}' factor for system '{}' in network '{}'".format(cost, component, netname))
                    except:
                        print("System '{}' not found in network '{}'".format(component,netname))
                  
                
        # Solve power flow and optimize for monetary cost
        my_smartgrid.solve(
             primary_energy_weight=weights["energy"],
             ghg_emissions_weight=weights["ghg"],
             monetary_weight=weights["monetary"],
             split_by_month = split_by_month,
             solver = solver
        )
         
        smartgrid_file_path = os.path.join(output_folder_path, "{}".format(year))
        my_smartgrid.save(smartgrid_file_path)
        os.mkdir(smartgrid_file_path)
        my_smartgrid.save_summary(smartgrid_file_path)
        my_smartgrid.sankey_plot(os.path.join(smartgrid_file_path,"sankey.pdf"),2000,2000)
        my_smartgrid.plot(smartgrid_file_path)
        
        return year, smartgrid_file_path

if __name__ == "__main__":
    main(sys.argv[1:])