# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 14:53:59 2019

@author: g.peronato
"""
import networkx as nx
import geopandas as gpd
import matplotlib.pyplot as plt
from shapely.ops import split, snap, linemerge
from shapely.geometry import Point
import pandas as pd
import numpy as np

def splitLine(gdf_line, gdf_points, tolerance=0.1):
    """
    Split the union of lines with the union of points resulting 
    Parameters
    ----------
    gdf_line : geoDataFrame
        geodataframe with multiple rows of lines
    gdf_points : geoDataFrame
        geodataframe with multiple rows of single points

    Returns
    -------
    gdf_segments : geoDataFrame
        geodataframe of segments
    """
    list_segments = []
    for i, line in gdf_line.iterrows():
        coords = gdf_points.geometry.unary_union
    
        # snap and split coords on line
        # returns GeometryCollection
        split_line = split(line.geometry, snap(coords, line.geometry, tolerance))
        
        # transform Geometry Collection to GeoDataFrame
        segments = [feature for feature in split_line]
        
        # merge the first and last segment of a ring
        # if there is more than one segment
        # if the first vertex is not a splitting point
        if len(segments) > 1 and not Point(segments[0].coords[0]) in gdf_points.geometry:
            segments = [linemerge([segments[-1],segments[0]])] + segments[1:-1]

        # create a GeoDataFrame from the list of segments and the set the original ID
        gdf_segments = gpd.GeoDataFrame(list(range(len(segments))), geometry=segments)
        gdf_segments["oID"] = i
        list_segments.append(gdf_segments)

    segments = pd.concat(list_segments)
    segments = segments.rename(columns={0: "segID"})

    return segments

#def explode(lines):
#    exploded = []
#    for l, line in lines.iterrows():
#        print(len(line.geometry.coords))
#        for p in range(len(line.geometry.coords)-1):
#            segment = line.copy()
#            segment.name = 0
#            segment["geometry"] = LineString([line.geometry.coords[p],line.geometry.coords[p+1]])
#            exploded.append(gpd.GeoDataFrame(segment))
#    exploded = gpd.GeoDataFrame(pd.concat(exploded,axis=1,ignore_index=True).transpose())
#    return exploded
#            
def findIntersections(network,points):
    network["int1"] = np.nan
    network["int2"] = np.nan
    for s, segment in network.iterrows():
        for vertex in segment.geometry.coords:
            for p, point in points.iterrows():
                if vertex == point.geometry.coords[0]:
                    if not np.isnan(network.loc[s,"int1"]):
                        network.loc[s,"int2"] = p
                    else:
                        network.loc[s,"int1"] = p
    if network[["int1","int2"]].isnull().values.any():
        print("Warning: every network segment must have at least two intersecting points")
    return network

def createGraph(network,points):
    G = nx.DiGraph()
    for n, node in points.iterrows():
        G.add_node(str(int(n)),power = node.power)
#        print(n,node.power)
    for l in network.oID.unique():
        segments = network[network.oID == l]
        for s, segment in segments.iterrows():
            if not np.isnan(segment.int2):
                G.add_edge(str(int(segment.int2)), str(int(segment.int1)), length=int(segment.geometry.length))
#                print(str(int(segment.int2)), str(int(segment.int1)), int(segment.geometry.length))
    flowCost, flowDict = nx.network_simplex(G, demand = 'power',
                                             capacity = 'capacity',
                                             weight = 'length')
    return flowDict

def graphToPower(network, flowdict):
    network["power"] = np.nan
    flowdict = pd.DataFrame(flowdict)
    for s, segment in network.iterrows():
        network.loc[s,"power"] = flowdict.loc[str(int(segment.int1)),str(int(segment.int2))]
    return network


if __name__ == "__main__":
    #path = r"S:\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\reseau\geometrie\\"
    path = "sampledata/"
    network = gpd.read_file(path+"reseau-test2.gpkg")
    exchangers = gpd.read_file(path+"soustations-test.gpkg")
    #buildings = gpd.read_file("volumes.gpkg")
    
    
    segments = splitLine(network,exchangers, 0)
    
    network = pd.merge(network,segments,left_index=True,right_on="oID")
    network = network.drop("geometry_x",axis=1)
    network = network.rename(columns={"geometry_y":"geometry"})
    network = network.reset_index()
    network = network.drop("index",axis=1)
    
    network = findIntersections(network,exchangers)
    
    
    # Plotting network
    #fig, ax = plt.subplots() 
    #
    ##network.plot(ax=ax,column="oID",linewidth=10)
    #network.plot(ax=ax,column="segID",linewidth=3)
    #
    #exchangers.plot(ax=ax,color="red",markersize=30)
    #exchangers.apply(lambda x: ax.annotate(s=x.name, xy=x.geometry.coords[0], ha='center', color="k"),axis=1)
    

    #network.apply(lambda x: ax.annotate(s=x.segID, xy=x.geometry.coords[1], ha='center'),axis=1);
    
    
    flowdict = createGraph(network,exchangers)   
    network = graphToPower(network, flowdict)
    
    # Plotting results
    fig, ax = plt.subplots() 
    network.plot(ax=ax,color="grey")
    network.plot(ax=ax,color="black",linewidth=network.power)
    
    exchangers[exchangers.power > 0].plot(ax=ax,color="red",markersize=exchangers[exchangers.power > 0].power.abs()*100)
    exchangers[exchangers.power < 0].plot(ax=ax,color="green",markersize=exchangers[exchangers.power < 0].power.abs()*100)
    
    exchangers.apply(lambda x: ax.annotate(s=x.name, xy=x.geometry.coords[0], ha='center', color="k"),axis=1)
    
    

    #G = nx.DiGraph()
    #G.add_node('a', power = -1)
    #G.add_node('b', power = -1)
    #G.add_node('c', power = 0)
    #G.add_node('d', power = 2)
    #G.add_edge('a', 'c', length = 4)
    #G.add_edge('b', 'c', length = 1)
    #G.add_edge('c', 'd', length = 2)
    #G.add_edge('a', 'b', length = 1)
    #flowCost, flowDict = nx.network_simplex(G, demand = 'power',
    #                                         capacity = 'capacity',
    #                                         weight = 'length')
    #flowDict
