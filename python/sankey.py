


import pandas as pd





# Sankey
sink_list = [n for n, v in smartgrid.flow_based.G.nodes(data=True) if v['is_sink'] is True]


edge_list = []

for sink in sink_list:

	flows = smartgrid.flow_based.get_mix(sink, 'source', relative=False)
	nodes = flows.columns
	nodes = [n for n in nodes if n != 'time']
	
	print(nodes)
	
	for i in range(len(nodes)):
		
		steps = nodes[i].split('<')
		
		steps.reverse()
		
#		# Consumption
#		if 'cooling' not in steps[len(steps)-1]:
#			steps.reverse()
#			
#		# Recuperation
#		elif 'hot_water' in steps[0] or 'heating' in steps[0]:
#			steps.reverse()
#			steps = steps[2:(len(steps)-1)] # remove load and bus steps
			
			
		if any(['electricity' in s for s in steps]):
			vector = 'electricity'
		elif any(['cooling' in s for s in steps]):
			vector = 'cooling'
		elif any(['hot_wate' in s for s in steps]):
			vector = 'hot_water'
		else:
			vector = 'heating'
		
		# Remove building id
		# steps = ['.'.join(s.split('.')[1:3]) for s in steps]
		
		for j in range(len(steps)-1):
			edge_list.append({'from': steps[j], 'to': steps[j+1], 'volume': flows[nodes[i]].sum(), 'vector': vector})
			
#		edge_list.append({'from': suit[len(suit)-1], 'to': vector, 'volume': flows[nodes[i]].sum(), 'vector': vector})
#		edge_list.append({'from': vector, 'to': building, 'volume': flows[nodes[i]].sum(), 'vector': vector})










building_ids = pd.read_excel('S:/ECT_LC/Elioth/20_Projets/BAOA129 - Bruneseau Equipe Hardel/21 - SmartGrid/2 - Calc/data/consommation/geometrie/in/6/programme.xlsx')
building_ids = building_ids['building_id'].unique().tolist()
building_ids.append('Unique')

flows = pd.read_csv('D:\\DATA\\F.POUCHAIN\\dev\\performance\\data\\flows.csv')
flows = flows.to_dict('records')

edge_list = []

import re

for flow in flows:
	
	steps = flow['sink'].split('<')		
	
#	if 'tfp' in flow['sink'] and 'elec' in flow['sink'] and 'load' in steps[len(steps)-1]:
#		print(flow)
#		print(steps)
	
	
	steps.reverse()
	
	if 'elec_load' not in steps[0]:
		
		if any(['elec' in s for s in steps]):
			vector = 'electricity'
		elif any(['cool' in s for s in steps]):
			vector = 'cooling'
		elif any(['hot_wate' in s for s in steps]):
			vector = 'hot_water'
		else:
			vector = 'heating'
		
		# Remove building id
#		steps = [re.sub('|'.join(building_ids), '', s) for s in steps]
#		
#		steps = [s for s in steps if 'con' not in s or 'hp' in s]
#		steps = [s for s in steps if 'loop' not in s]
		
		for j in range(len(steps)-1):
			edge_list.append({'from': steps[j], 'to': steps[j+1], 'volume': flow['power'], 'vector': vector})





import pandas as pd

edges = pd.DataFrame.from_dict(edge_list)
edges = edges.groupby(['from', 'to', 'vector'], as_index=False)['volume'].sum()

edges = edges.loc[edges['from'] != edges['to']]

nodes = edges['from'].unique().tolist() + edges['to'].unique().tolist()
nodes = list(set(nodes))

edges_index = pd.DataFrame({'from': nodes, 'to': nodes})
edges_index['from_index'] = np.arange(0, edges_index.shape[0])
edges_index['to_index'] = np.arange(0, edges_index.shape[0])

edges = pd.merge(edges, edges_index[['from', 'from_index']], on = 'from')
edges = pd.merge(edges, edges_index[['to', 'to_index']], on = 'to')

colors = pd.DataFrame({'vector': ['electricity', 'heating', 'hot_water', 'cooling'], 'color': ['#f1c40f', '#e74c3c', '#c0392b', '#3498db']})
colors['rgba_0.25'] = colors['color'].apply(lambda x: 'rgba(' + ','.join([str(int(x.lstrip('#')[i:i+2], 16)) for i in (0, 2, 4)]) + ',0.5)')
colors['rgba_0.75'] = colors['color'].apply(lambda x: 'rgba(' + ','.join([str(int(x.lstrip('#')[i:i+2], 16)) for i in (0, 2, 4)]) + ',0.75)')
edges = pd.merge(edges, colors, on = 'vector')

#nodes_vector = edges.groupby('from', as_index=False)['vector'].first()
#edges_index = pd.merge(edges_index, nodes_vector, on = 'from')
#edges_index = pd.merge(edges_index, colors, on = 'vector')
#edges_index = edges_index.sort_values('from_index', ascending=False)

import plotly.graph_objects as go

fig = go.Figure(data=[go.Sankey(
    node = dict(
      pad = 15,
      thickness = 10,
      line = dict(color = "white", width = 0.5),
      label = edges_index['from'],
	  color = '#34495e'
    ),
    link = dict(
      source = edges['from_index'],
      target = edges['to_index'],
      value = edges['volume'],
	  color = edges['rgba_0.25']
  ))])

fig.update_layout(font_size=10, width = 1000, height = 1000)

fig.write_image("D:\\DATA\\F.POUCHAIN\\dev\\smartgrid\\data\\fig1.png")
